﻿using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models;
using RedBull.Entity.Models.Logs;
using RedBull.UdpServer;
using RedBull.UdpServer.Udp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ServiceProcess;
using System.Configuration;

namespace RedBullUdpServer.Service
{
    static class AppConfiguration
    {
        public static bool IsMysqlType()
        {
            return (ConfigurationManager.AppSettings["database:type"] ?? "mssql").ToLower() == "mysql";
        }
    }

    #if WINDOWS
        public static class ServiceStarter
        {
            public const string ServiceName = "RedBullServer";

            class Service : ServiceBase
            {
                public Service()
                {
                    ServiceName = ServiceStarter.ServiceName;
                }

                protected override void OnStart(string[] args)
                {
                    ServiceStarter.Start(args);
                }

                protected override void OnStop()
                {
                    ServiceStarter.Stop();
                }
            }

            static void Main(string[] args)
            {
                var dbMysqlType = AppConfiguration.IsMysqlType();

                if (!Environment.UserInteractive) // running as service
                {
                    using (var service = new Service())
                        ServiceBase.Run(service);
                }
                else
                {
                    var container = new UnityContainer();
                    new RedBull.Domain.DependencySupport.RegisterTypes(container).BuildUnityContainer(dbMysqlType);

                    var program = container.Resolve<Reporter>();
                    program.Initlialise();
                }
            }

            private static void Start(string[] args)
            {
                var dbMysqlType = AppConfiguration.IsMysqlType();

                var container = new UnityContainer();
                new RedBull.Domain.DependencySupport.RegisterTypes(container).BuildUnityContainer(dbMysqlType);

                var program = container.Resolve<Reporter>();
                program.Initlialise();
            }

            private static void Stop()
            {
                // sending mail to administrator that server service was stopped
            }
        }
    #endif

    #if !WINDOWS
        class MonoService
        {
            static void Main(string[] args)
            {
                var dbMysqlType = AppConfiguration.IsMysqlType();

                var container = new UnityContainer();
                new RedBull.Domain.DependencySupport.RegisterTypes(container).BuildUnityContainer(dbMysqlType);

                var program = container.Resolve<Reporter>();
                program.Initlialise();
            }
        }
    #endif
}
