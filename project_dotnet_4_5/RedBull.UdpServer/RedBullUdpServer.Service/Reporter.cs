﻿using Hangfire;
using Hangfire.MySql;
using Hangfire.SqlServer;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using RedBull.Common;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models;
using RedBull.Entity.Models.Logs;
using RedBull.Mailing;
using RedBull.UdpServer.Udp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RedBullUdpServer.Service
{
    public class Reporter
    {
        private BackgroundJobServer _backgroundJobServer;

        readonly IReportService _reportService;
        readonly IFridgeService _fridgeService;
        readonly IErrorServices _errorServices;
        readonly ILogger _logger;

        public Reporter(IReportService _reportService, IFridgeService _fridgeService, IErrorServices _errorServices, ILogger logger)
        {
            this._reportService = _reportService;
            this._fridgeService = _fridgeService;
            this._errorServices = _errorServices;
            this._logger = logger;
        }

        /// <summary>
        /// Initialise program method
        /// </summary>
        public void Initlialise()
        {
            _logger.Log("Redbull UdpServer");

            StartServer();
            //InitialiseHangfire();

            Console.ReadLine();
        }

        /// <summary>
        /// Server starting method
        /// </summary>
        public void StartServer()
        {
            var server = new UdpServer(9923, (response) =>
            {
                ClientReport clientReport = new ClientReport();

                try
                {
                    clientReport = JsonConvert.DeserializeObject<ClientReport>(response);

                    _logger.Log(string.Format("RESPONSE: FRIDGE: {0}, CANS: {1}, WEIGHT: {2}", clientReport.Id, clientReport.Cans, clientReport.Weight));

                    
                    Fridge fridge = _fridgeService.GetFridgeById(clientReport.Id);

                    if (fridge == null)
                    {
                        _logger.Log("ERROR: Fridge not found");
                        CreateError("Niezarejestrowana lodówka podjęła próbę zapisania danych", Convert.ToInt32(clientReport.Id));
                        return new UdpResult(UdpResultStatus.ERROR);
                    }

                    /*
                    if (clientReport.Cans > fridge.Capacity)
                    {
                        _logger.Log("ERROR: Capacity exceeded");
                        CreateError("Lodówka zgłosiła liczbę puszek większą od swojej pojemności", clientReport.Id);
                        return new UdpResult(UdpResultStatus.ERROR);
                    }
                    */

                    _reportService.MakeReport(fridge,clientReport.Cans);

                    fridge.LastActivity = DateTime.Now;

                    if (fridge.FridgeError)
                    {
                        fridge.FridgeError = false;
                        fridge.IsActive = true;
                    }

                    _fridgeService.Update(fridge);

                    return new UdpResult(UdpResultStatus.OK);
                }
                catch (Exception ex)
                {
                    _logger.Log(string.Format("ERROR: {0}", ex.Message));

                    CreateError(ex.Message.ToString(),Convert.ToInt32(clientReport.Id));
                    return new UdpResult(UdpResultStatus.ERROR);
                }
            }, _logger);

            server.Start();
        }

        /// <summary>
        /// Hangfire Initialisator
        /// </summary>
        public void InitialiseHangfire()
        {
            var container = new UnityContainer();
            var dbMysqlType = AppConfiguration.IsMysqlType();

            new RedBull.Domain.DependencySupport.RegisterTypes(container).BuildUnityContainer(dbMysqlType);

            var program = container.Resolve<Event>();

            if (dbMysqlType)
            {
                GlobalConfiguration.Configuration.UseStorage(new MySqlStorage("RedBull_DB_ConnectionString", 
                    new MySqlStorageOptions()
                    {
                    QueuePollInterval = TimeSpan.FromSeconds(15)
                    }));
            }
            else
            {
                GlobalConfiguration.Configuration.UseSqlServerStorage("RedBull_DB_ConnectionString");
            }
            
            GlobalConfiguration.Configuration.UseActivator(new ContainerJobActivator(container));

            _backgroundJobServer = new BackgroundJobServer();

            RecurringJob.AddOrUpdate(() => program.CountSalesAvarage(), Cron.Daily);
            RecurringJob.AddOrUpdate(() => program.LookForError(), Cron.Minutely);
            RecurringJob.AddOrUpdate(() => program.LookForEmptyFridge(), Cron.Minutely);
        }

        public class ContainerJobActivator : JobActivator
        {
            private IUnityContainer _container;

            public ContainerJobActivator(IUnityContainer container)
            {
                _container = container;
            }

            public override object ActivateJob(Type type)
            {
                return _container.Resolve(type);
            }
        }

        /// <summary>
        /// Error Creator
        /// </summary>
        /// <param name="error">Error message</param>
        /// <param name="fridgeId">Fridge name</param>
        public void CreateError(string error, int fridgeId)
        {
            _errorServices.Create(new ErrorLog() { ErrorText = error, FridgeRefId = fridgeId });
        }

    }
}
