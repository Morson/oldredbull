﻿using Microsoft.Owin.Logging;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models.Logs;
using RedBull.Mailing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBullUdpServer.Service
{
    public class Event
    {
        readonly IReportService _reportService;
        readonly IFridgeService _fridgeService;
        readonly IErrorServices _errorServices;
        readonly ISmsOutboxService _smsOutboxService;

        public Event(IReportService _reportService, IFridgeService _fridgeService, IErrorServices _errorServices,
            ISmsOutboxService _smsOutboxService)
        {
            this._reportService = _reportService;
            this._fridgeService = _fridgeService;
            this._errorServices = _errorServices;
            this._smsOutboxService = _smsOutboxService;
        }

        /// <summary>
        /// Lookings for fridges errors
        /// </summary>
        public void LookForError()
        {
            var fridgesList = _fridgeService.GetAll();

            Mail mail = new Mail();

            foreach (var fridge in fridgesList)
            {
                int salesAvarageFromLastTwoWeeks = fridge.SallesAvaragePerDay;

                if (salesAvarageFromLastTwoWeeks == 0)
                {
                    salesAvarageFromLastTwoWeeks = 2;
                }

                if ((DateTime.Now - fridge.LastActivity).Minutes > 6 && fridge.IsActive)
                {
                    fridge.IsActive = false;
                    fridge.FridgeError = true;

                    CreateError("Utracone połączenie z lodówką, Lodówka jest offline", fridge.Id);
                    /*mail.SentMessage("jacek.piatkowski@havasww.com", EmailTypesEnum.ErrorMail, _fridgeService.GetAll().First(), "", "Utracone połączenie z lodówką, Lodówka jest offline");
                    mail.SentMessage("robert.borkowski@havasww.com", EmailTypesEnum.ErrorMail, _fridgeService.GetAll().First(), "", "Utracone połączenie z lodówką, Lodówka jest offline");
                    mail.SentMessage("piotr.kosiakowski@havasww.com", EmailTypesEnum.ErrorMail, _fridgeService.GetAll().First(), "", "Utracone połączenie z lodówką, Lodówka jest offline");
                    mail.SentMessage("dominik.komar@havasmg.com", EmailTypesEnum.ErrorMail, _fridgeService.GetAll().First(), "", "Utracone połączenie z lodówką, Lodówka jest offline");
                    mail.SentMessage("mikolaj.czajkowski@havasww.com", EmailTypesEnum.ErrorMail, _fridgeService.GetAll().First(), "", "Utracone połączenie z lodówką, Lodówka jest offline");*/
                }

                int? numberOfCans = _reportService.GetNumberOdCans(fridge.Id);

                if (numberOfCans <= (salesAvarageFromLastTwoWeeks*3) && !fridge.IsEmpty)
                {
                    /*mail.SentMessage("jacek.piatkowski@havasww.com", EmailTypesEnum.EmptyLogMail, _fridgeService.GetAll().First(), numberOfCans.ToString(), "");
                    mail.SentMessage("robert.borkowski@havasww.com", EmailTypesEnum.EmptyLogMail, _fridgeService.GetAll().First(), numberOfCans.ToString(), "");
                    mail.SentMessage("piotr.kosiakowski@havasww.com", EmailTypesEnum.EmptyLogMail, _fridgeService.GetAll().First(), numberOfCans.ToString(), "");
                    mail.SentMessage("mikolaj.czajkowski@havasww.com", EmailTypesEnum.EmptyLogMail, _fridgeService.GetAll().First(), numberOfCans.ToString(), "");
                    mail.SentMessage("dominik.komar@havasmg.com", EmailTypesEnum.EmptyLogMail, _fridgeService.GetAll().First(), numberOfCans.ToString(), "");*/
                    fridge.IsEmpty = true;
                }

                if (numberOfCans > (salesAvarageFromLastTwoWeeks*3) && fridge.IsEmpty)
                {
                    fridge.IsEmpty = false;
                }

                _fridgeService.Update(fridge);
            }
        }

        /// <summary>
        /// Count sales avarage
        /// </summary>
        public void CountSalesAvarage()
        {
            var fridgesList = _fridgeService.GetAll();

            foreach (var fridge in fridgesList)
            {
                fridge.SallesAvaragePerDay = _reportService.GetLastTwooWeekNumberOfCansAvarage(fridge.Id);

                _fridgeService.Update(fridge);
            }
        }

        /// <summary>
        /// Error Creator
        /// </summary>
        /// <param name="error">Error message</param>
        /// <param name="fridgeId">Fridge name</param>
        public void CreateError(string error, int fridgeId)
        {
            _errorServices.Create(new ErrorLog() {ErrorText = error, FridgeRefId = fridgeId});
        }


        public void LookForEmptyFridge()
        {
            var fridges = _fridgeService.GetAlmostEmptyFridges().ToList();
            foreach (var fridge in fridges)
            {
                _smsOutboxService.SendSms(fridge);
            }
        }
    }
}
