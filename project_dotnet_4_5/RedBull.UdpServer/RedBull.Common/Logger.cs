﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Common
{
    public class Logger : ILogger
    {
        public void Log(string message)
        {
            var curentDate = DateTime.Now;

            Console.WriteLine("[{0}]: {1}", curentDate, message);
        }
    }
}
