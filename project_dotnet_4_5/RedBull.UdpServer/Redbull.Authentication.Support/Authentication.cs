﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using Redbull.Authentication.Support.Models;
using Redbull.Authentication.Support.Models.Enums;
using Redbull.Authentication.Support.Models.Mapping;

namespace Redbull.Authentication.Support
{
    public static class Authentication
    {
        public static CurrentUserInfo GetCurrentUserInfo()
        {
            var cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (cookie == null) return new CurrentUserInfo();

            FormsAuthenticationTicket formsAuthenticationTicket = FormsAuthentication.Decrypt(cookie.Value);

            return new CurrentUserInfo(
                formsAuthenticationTicket.Name,
                formsAuthenticationTicket.UserData.toEnum(),
                formsAuthenticationTicket.IsPersistent,
                cookie.Value);
        }

        public static void LoginUser(string login, string roles, bool isRemember)
        {
            var userInfo = CreateUserInfo(login, roles.toEnum(), isRemember);
            LogOutUser();
            Authenticate(userInfo);
        }

        public static void LoginUser(string login, UserRoleEnum role, bool isRemember)
        {
            var roles = new List<UserRoleEnum>() { role };
            var userInfo = CreateUserInfo(login, roles, isRemember);
            LogOutUser();
            Authenticate(userInfo);
        }

        public static void LogOutUser()
        {
            var user = GetCurrentUserInfo();
            if (user.Login != null)
            {
                var ticket = createFormsAuthenticationTicket(user);
                var encrypted = FormsAuthentication.Encrypt(ticket);
                expireCookie(FormsAuthentication.FormsCookieName, encrypted, FormsAuthentication.FormsCookiePath);
                LogOut();
            }
        }

        private static void LogOut()
        {
            FormsAuthentication.SignOut();
        }

        private static CurrentUserInfo CreateUserInfo(string login,
            ICollection<UserRoleEnum> roles,
            bool isRemember)
        {
            return new CurrentUserInfo(login, roles, isRemember, null);
        }

        private static void Authenticate(CurrentUserInfo currentUserInfo)
        {
            var ticket = createFormsAuthenticationTicket(currentUserInfo);
            var encrypted = FormsAuthentication.Encrypt(ticket);
            createCookie(FormsAuthentication.FormsCookieName, encrypted, FormsAuthentication.FormsCookiePath);
        }

        private static FormsAuthenticationTicket createFormsAuthenticationTicket(CurrentUserInfo currentUserInfo)
        {
            return new FormsAuthenticationTicket(
                    1,                                                                      // version
                    currentUserInfo.Login,                                                  // user name
                    DateTime.Now,                                                           // created
                    DateTime.Now.AddMinutes(currentUserInfo.MinutesOfExpirationTime),       // expires
                    currentUserInfo.IsRemember,                                             // persistent?
                    currentUserInfo.Roles.toStr(),                                          // can be used to store roles
                    FormsAuthentication.FormsCookiePath
                    );
        }

        private static void createCookie(string name, string value, string path)
        {
            HttpCookie cookie = new HttpCookie(name, value);
            cookie.Path = path;
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        private static void expireCookie(string name, string value, string path)
        {
            HttpCookie cookie = new HttpCookie(name, value);
            cookie.Path = path;
            cookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

    }
}
