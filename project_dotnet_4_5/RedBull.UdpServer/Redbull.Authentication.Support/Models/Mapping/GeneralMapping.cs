﻿using System;
using System.Collections.Generic;
using System.Linq;
using Redbull.Authentication.Support.Models.Enums;

namespace Redbull.Authentication.Support.Models.Mapping
{
    public static class GeneralMapping
    {
        public static string toStr(this ICollection<UserRoleEnum> roles)
        {
            return string.Join(";", roles.ToArray());
        }

        public static string[] toStrArray(this string roles)
        {
            return roles.Split(';');
        }

        public static ICollection<UserRoleEnum> toEnum(this string roles)
        {
            return roles.Split(';').ToList().Select(s => (UserRoleEnum)Enum.Parse(typeof(UserRoleEnum), s)).ToList();
        }
    }
}
