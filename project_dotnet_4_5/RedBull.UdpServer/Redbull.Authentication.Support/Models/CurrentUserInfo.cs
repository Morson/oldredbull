﻿using System.Collections.Generic;
using Redbull.Authentication.Support.Models.Enums;

namespace Redbull.Authentication.Support.Models
{
    public class CurrentUserInfo
    {
        public string Login { get; private set; }
        public ICollection<UserRoleEnum> Roles { get; private set; }
        public bool IsRemember { get; private set; }
        public int MinutesOfExpirationTime { get; private set; }
        public string CookieTicket { get; set; }


        public CurrentUserInfo()
        {
            this.Login = null;
        }

        public CurrentUserInfo(
            string login,
            ICollection<UserRoleEnum> roles,
            bool isRemember,
            string CookieTicket)
        {
            this.Login = login;
            this.Roles = roles;
            this.IsRemember = getRemember(isRemember, roles);
            this.MinutesOfExpirationTime = getExpirationTime(roles);
            this.CookieTicket = CookieTicket;

        }

        private bool getRemember(bool isRemember, ICollection<UserRoleEnum> roles)
        {
            if (roles.Contains(UserRoleEnum.Admin)) return false;
            else return isRemember;
        }

        private int getExpirationTime(ICollection<UserRoleEnum> roles)
        {
            if (roles.Contains(UserRoleEnum.Admin)) return 15; //15minutes
            else return 10080; //year
        }
    }
}
