﻿using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Practices.Unity;
using RedBull.Web.Core;
using RedBullUdpServer.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace RedBull.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(System.Web.Http.GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            if (WebAppConfiguration.IsMysqlType())
            {
                new LoggerManager().InitMySql();
            }
            else
            {
                new LoggerManager().InitSqlServer();
            }
            System.Web.Http.GlobalConfiguration.Configuration.EnsureInitialized();
            
            InitialiseHangfire();
          
        }

        /// <summary>
        /// Hangfire Initialiser
        /// </summary>
        public void InitialiseHangfire()
        {

            var container = new UnityContainer();
            var dbMysqlType = WebAppConfiguration.IsMysqlType();

            new RedBull.Domain.DependencySupport.RegisterTypes(container).BuildUnityContainer(dbMysqlType);

            var program = container.Resolve<Event>();
            
                Hangfire.GlobalConfiguration.Configuration.UseSqlServerStorage("RedBull_DB_ConnectionString", new SqlServerStorageOptions
                {
                    QueuePollInterval = TimeSpan.FromSeconds(15),
                    PrepareSchemaIfNecessary = false,
                    SchemaName = "dbo"
                });

            Hangfire.GlobalConfiguration.Configuration.UseActivator(new RedBullUdpServer.Service.Reporter.ContainerJobActivator(container)); 

            var server = new BackgroundJobServer();
            
            //RecurringJob.AddOrUpdate(() => program.CountSalesAvarage(), Cron.Daily);
            //RecurringJob.AddOrUpdate(() => program.LookForError(), Cron.Minutely);
            RecurringJob.AddOrUpdate(() => program.LookForEmptyFridge(), Cron.Minutely);

        }
         
    }
}