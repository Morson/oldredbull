﻿using System.ComponentModel.DataAnnotations;

namespace RedBull.Web.Models
{
    public class LoginBindingModel
    {
        [Required(ErrorMessage = "Prosze podać Logi")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Prosze podać Hasło")]
        public string Password { get; set; }
    }
}