﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedBull.Web.Models
{
    public class SmsRaportCallback
    {
        public string MsgId { get; set; }
        public string status { get; set; }
        public string status_name { get; set; }
        public string idx { get; set; }
        public string donedate { get; set; }
        public string username { get; set; }
        public string points { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string mcc { get; set; }
        public string mnc { get; set; }
    }
}