﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedBull.Web.Models
{
    public class SmsInboxCallback
    {
        public string sms_to { get; set; }
        public string sms_from { get; set; }
        public string sms_text { get; set; }
        public string sms_date { get; set; }
        public string username { get; set; }
        public string MsgId { get; set; }
    }
}