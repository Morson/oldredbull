﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedBull.Web.Models
{
    public class FridgeDTO
    {
        public int? NumberOfCans { get; set; } //number od cans inside
        public int FridgeId { get; set; } //Unique fridge number
        public DateTime LastActivity { get; set; } //DateTime of last fillity report
        public bool FridgeError { get; set; } //If problem with fridge occured
        public bool IsActive { get; set; } //Disabled or enabled
        public int Capacity { get; set; } //Capacity
        public string ErrorText { get; set; } //error text
        public int SallesAvaragePerDay { get; set; }
        public string FridgeDescription { get; set; }
        public string DateTimeFromString { get; set; }
        public string DateTimeToString { get; set; }
        public int timeSpan { get; set; }
        public int? FridgeOwnerRefId { get; set; }
        public string labels { get; set; }
        public string values { get; set; }
        public string ClientName { get; set; }
        public string FridgeManagerName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string GoogleLocalization { get; set; }
        public string EmailAddress { get; set; }
        public TimeSpan? LastActivityElapsed
        {
            get { return DateTime.Now - LastActivity; }
        }

        public string QueryFormattedAdress
        {
            get { return Address.Replace(' ', '+'); }
        }
    }
}