﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedBull.Web.Models
{
    public class ChartReportDTO
    {
        public DateTime x { get; set; }
        public int y { get; set; }
    }
}