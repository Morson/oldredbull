﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedBull.Web.Models
{
    public class ErrolLogDTO
    {
        public string ErrorText { get; set; }
        public int FridgeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; } //Disabled or enabled
    }
}