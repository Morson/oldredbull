﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedBull.Web.Models
{
    public class ReportParams
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public int scale { get; set; }
    }
}