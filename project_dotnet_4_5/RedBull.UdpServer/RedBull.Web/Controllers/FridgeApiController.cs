﻿using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models;
using RedBull.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using RedBull.Domain.Enums;
using RedBullUdpServer.Service;

namespace RedBull.Web.Controllers
{
    public class FridgeApiController : ApiController
    {

        readonly IReportService _reportService;
        readonly IFridgeService _fridgeService;
        readonly ISmsInboxService _smsInboxService;
        readonly ISmsOutboxService _smsOutboxService;
        readonly IErrorServices _errorService;
        private readonly ICapsAmountCalculatorService _calculatorService;

        public FridgeApiController(IReportService _reportService, IFridgeService _fridgeService,
            ISmsInboxService _smsInboxService, ISmsOutboxService _smsOutboxService, IErrorServices _errorService,
            ICapsAmountCalculatorService _calculatorService)
        {
            this._reportService = _reportService;
            this._fridgeService = _fridgeService;
            this._smsInboxService = _smsInboxService;
            this._smsOutboxService = _smsOutboxService;
            this._errorService = _errorService;
            this._calculatorService = _calculatorService;
        }


        // POST api/<controller>
        [HttpPost, Route("api/FridgeApi/GetModels")]
        public IEnumerable<ChartReportDTO> GetModels([FromBody] ReportParams paramsList)
        {
            var result = _reportService.GenerateGroupReport(1, paramsList.fromDate, paramsList.toDate);
            var reports = new List<ChartReportDTO>();

            result.ToList().ForEach(p =>
            {
                var tempReport = new ChartReportDTO
                {
                    x = p.CreatedDate,
                    y = p.NumberOfCans
                };

                reports.Add(tempReport);
            });

            return reports;
        }

        [HttpGet, Route("api/FridgeApi/Testowa")]
        public IHttpActionResult Testowa()
        {
            var fridges = _fridgeService.GetAlmostEmptyFridges().ToList();
            foreach (var fridge in fridges)
            {
                _smsOutboxService.SendSms(fridge);
            }
            return Ok();
        }

        //sms api wysyla requesta na ta metode jezeli ktos odpowiedzial na smsa
        [HttpPost, Route("api/FridgeApi/SmsInbox")]
        public HttpResponseMessage SmsInbox([FromBody] SmsInboxCallback sms)
        {
            var nextState = _smsInboxService.CreateSms(sms.MsgId, sms.sms_text);
            if (nextState != null)
                _smsOutboxService.SendSms((SmsNextStateEnum) nextState, sms.MsgId);

            return new HttpResponseMessage()
            {
                Content = new StringContent(
                    "OK",
                    Encoding.UTF8,
                    "text/html"
                    )
            };
        }

        //metoda testowa wymagana przez smsapi
        [HttpGet, Route("api/FridgeApi/SmsInbox")]
        public HttpResponseMessage SmsInbox()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(
                    "OK",
                    Encoding.UTF8,
                    "text/html"
                    )
            };
        }

        //sms api wysyla raport doreczenia na ta metode
        [HttpGet, Route("api/FridgeApi/SmsRaport")]
        public HttpResponseMessage SmsRaport(string MsgId = null, string to = null, string from = null, string donedate = null, string status = null,
            string status_name = null, string idx = null, string username = null, string points = null, string mcc = null, string mnc=null)
        {
            if (!String.IsNullOrEmpty(MsgId))
            {
                _smsOutboxService.UpdateStatus(MsgId, status);
            }
           
            return new HttpResponseMessage()
            {
                Content = new StringContent(
                    "OK",
                    Encoding.UTF8,
                    "text/html"
                    )
            };
        }

        [HttpGet, Route("api/FridgeApi/SaveByGet")]
        public IHttpActionResult SaveByGet(int Id, int Cans, int Weight)
        {
            Fridge fridge = _fridgeService.GetFridgeById(Id);

            if (fridge == null)
            {
                return BadRequest("No fridge found.");
            }

            if (Cans > fridge.Capacity)
            {
                return BadRequest("Cans overflow");
            }

            //int calculatedWeight = _calculatorService.CalculateWeight(fridge,Cans,Weight);
            _reportService.MakeReport(fridge, Cans, Weight);//(fridge,Cans,Weight,ActualWeight);

            fridge.LastActivity = DateTime.Now;

            if (fridge.FridgeError)
            {
                fridge.FridgeError = false;
                fridge.IsActive = true;
            }

            _fridgeService.Update(fridge);

            return Ok();
        }

        [HttpPost, Route("api/FridgeApi/SaveData")]
        public IHttpActionResult SaveData([FromBody] ClientReport clientReport)
        {
            Fridge fridge = _fridgeService.GetFridgeById(clientReport.Id);

            if (fridge == null)
            {
                return BadRequest("No fridge found.");
            }

            if (clientReport.Cans > fridge.Capacity)
            {
                return BadRequest("Cans overflow");
            }
            
            _reportService.MakeReport(fridge, clientReport.Cans, clientReport.Weight);

            fridge.LastActivity = DateTime.Now;

            if (fridge.FridgeError)
            {
                fridge.FridgeError = false;
                fridge.IsActive = true;
            }

            _fridgeService.Update(fridge);

            return Ok();
        }
    }
}