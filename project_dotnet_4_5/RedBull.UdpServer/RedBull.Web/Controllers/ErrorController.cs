﻿using AutoMapper;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models.Logs;
using RedBull.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RedBull.Web.Controllers
{
    public class ErrorController : Controller
    {
        readonly IFridgeService _fridgeService;
        readonly IErrorServices _errorServices;

        public ErrorController(IFridgeService _fridgeService, IErrorServices _errorServices)
        {
            this._fridgeService = _fridgeService;
            this._errorServices = _errorServices;
        }

        [HttpGet]
        public ActionResult ShowErrorsList(int? fridgeId)
        {

            Mapper.CreateMap<ErrorLog, ErrolLogDTO>();

            List<ErrolLogDTO> model;
            if (fridgeId!=null)
            {
                model = Mapper.Map<List<ErrolLogDTO>>(_errorServices.GetAll().Where(error => error.FridgeRefId == fridgeId));
            }
            else
            {
                model = Mapper.Map<List<ErrolLogDTO>>(_errorServices.GetFridgeErrorsList());
            }

            if (model != null)
            {
                for (int i = 0; i < model.Count; i++)
                {
                    var fridge = _fridgeService.GetFridgeById(model[i].FridgeId);
                    model[i].IsActive = fridge.IsActive;
                }
            }


            return View(model);
        }

        [HttpPost]
        public ActionResult ShowErrorsList(IEnumerable<ErrolLogDTO> model)
        {
            return RedirectToAction("ShowErrorsList");
        }
    }
}
