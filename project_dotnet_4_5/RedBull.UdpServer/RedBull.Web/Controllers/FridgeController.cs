﻿using AutoMapper;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models;
using RedBull.Entity.Models.Logs;
using RedBull.Web.Core;
using RedBull.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RedBull.Web.Controllers
{
	public class FridgeController : Controller
	{
		readonly IReportService _reportService;
		readonly IFridgeService _fridgeService;
		readonly IErrorServices _errorServices;

		public FridgeController(IReportService _reportService, IFridgeService _fridgeService, IErrorServices _errorServices)
		{
			this._reportService = _reportService;
			this._fridgeService = _fridgeService;
			this._errorServices = _errorServices;
		}

		[HttpGet]
		public ActionResult ShowFridgesList()
		{
			Mapper.CreateMap<Fridge, FridgeDTO>().ForMember(dest =>dest.FridgeId,opts=>opts.MapFrom(src=>src.Id));
			Mapper.CreateMap<ErrorLog, ErrolLogDTO>().ForMember(dest =>dest.FridgeId,opts=>opts.MapFrom(src=>src.FridgeRefId));
			

			List<FridgeDTO> model = Mapper.Map<List<FridgeDTO>>(_fridgeService.GetAll());

			for (int i = 0; i < model.Count; i++)
			{
				if ((model[i].LastActivity - DateTime.Now).TotalMinutes >= 5)
				{
					model[i].FridgeError = true;
					_errorServices.Create(Mapper.Map<ErrorLog>(new ErrolLogDTO() { ErrorText = "Fridge is Offline", FridgeId = model[i].FridgeId }));
				}

				if (model[i].FridgeError)
				{
					model[i].ErrorText = _errorServices.GetLastFridgeError(model[i].FridgeId);
				}

				model[i].NumberOfCans = _reportService.GetNumberOdCans(model[i].FridgeId);
			}

			return View(model);
		}

		[HttpGet]
		//ShowFridge
		public ActionResult ShowFridges(string fridgeIdentity)
		{
			Mapper.CreateMap<Fridge, FridgeDTO>();
			int fridgeIntIdentity = Int32.Parse(fridgeIdentity);
			FridgeDTO model = Mapper.Map<FridgeDTO>(_fridgeService.GetFridgeById(fridgeIntIdentity));

			model.NumberOfCans = _reportService.GetNumberOdCans(model.FridgeId);

			model.LastActivity = _reportService.GetLastReport(fridgeIntIdentity).CreatedDate;

			//ViewBag.lastActivityMinutes = Convert.ToInt64((DateTime.Now - model.LastActivity).TotalMinutes).ToString();

			Dictionary<string, string> reports = new Dictionary<string, string>();

			DateTime dateFrom;
			DateTime dateTo;

			int timeSpan = 60;

			#region Chart starting date handler
			if (Session["ChartDataTimeFrom"] != null) //if chart starting date is choosed
			{
				dateFrom = (DateTime)Session["ChartDataTimeFrom"];
				//dateFrom = dateFrom.ToUniversalTime();
				model.DateTimeFromString = dateFrom.ToString();
				Session["ChartDataTimeFrom"] = null;
			}
			else
			{
				dateFrom = DateTime.Now.AddHours(-25);
			}
			#endregion

			#region Chart ending date handler
			if (Session["ChartDataTimeTo"] != null) //if chart ending date is choosed
			{
				dateTo = (DateTime)Session["ChartDataTimeTo"];
				//dateTo = dateTo.ToUniversalTime();
				model.DateTimeToString = dateTo.ToString();
				Session["ChartDataTimeTo"] = null;
			}
			else
			{
				dateTo = DateTime.Now;
			}
			#endregion

			#region Chart Time span handler
			if (Session["ChartTimeSpan"] != null)
			{
				//Zaokrąglamy do pełnych wartości. tj 15, 30 minut - następnie raport tworzymy dla tych wartości
				timeSpan = (int)Session["ChartTimeSpan"];
				model.timeSpan = timeSpan;
				if (model.timeSpan == 60) //jeżeli dokładność jest co do godziny
				{
					dateFrom = dateFrom.AddMinutes(-dateFrom.Minute);
				}
				else if (model.timeSpan == 15) //jeżeli dokładnosć jest co do 15 minut
				{
					if (dateFrom.Minute / 15 == 3) 
					{
						dateFrom = dateFrom.AddMinutes(-dateFrom.Minute);
						dateFrom = dateFrom.AddMinutes(45);
					}
					else if(dateFrom.Minute / 15 == 2)
					{
						dateFrom = dateFrom.AddMinutes(-dateFrom.Minute);
						dateFrom = dateFrom.AddMinutes(30);
					}
					else if (dateFrom.Minute / 15 == 1)
					{
						dateFrom = dateFrom.AddMinutes(-dateFrom.Minute);
						dateFrom = dateFrom.AddMinutes(15);
					}
					else
					{
						dateFrom = dateFrom.AddMinutes(-dateFrom.Minute);
					}
				}
				else if (model.timeSpan == 30) //jeżeli dokładność jest co do 30 minut
				{
					if (dateFrom.Minute / 30 == 1)
					{
						dateFrom = dateFrom.AddMinutes(-dateFrom.Minute);
						dateFrom = dateFrom.AddMinutes(30);
					}
					else
					{
						dateFrom = dateFrom.AddMinutes(-dateFrom.Minute);
					}
				}

				Session["ChartTimeSpan"] = null;
			}
			else
			{
				timeSpan = 60;
				dateFrom = dateFrom.AddMinutes(-dateFrom.Minute); //obcinamy minuty (zaokrąglamy do godzin)
			}
			#endregion
			
			model.labels = ""; //labels under the chart

			if (WebAppConfiguration.IsMysqlType())
			{
				return generateMySqlReport(model, timeSpan, dateFrom, dateTo);
			}
			
			int numberOfCircuits = (int)(dateTo - dateFrom).TotalHours * (60 / timeSpan); //number of loop courses

			DateTime time = dateFrom; //firts chart datatime

			for (int i = 0; i < numberOfCircuits; i++)
			{
				var numberOfCans = _reportService.GetNumberOdCans(time, model.FridgeId).ToString();

				if (String.IsNullOrEmpty(numberOfCans))
				{
					var lastReport = reports.LastOrDefault().Value;

					if (lastReport == null)
					{
						numberOfCans = "0";
					}
					else
					{
						numberOfCans = reports.Last().Value;
					}
				}
				reports.Add(time.ToString(), numberOfCans);
				model.labels = model.labels.Length == 0 ? String.Format(@"""{0}""", time.AddHours(2).ToString("HH:mm dd-MM-yyyy")) : String.Format(@"{0}, ""{1}""", model.labels, time.AddHours(2).ToString("HH:mm dd-MM-yyyy"));
				model.values = model.values == null ? String.Format("{0}", numberOfCans) : String.Format("{0}, {1}", model.values, numberOfCans);

				time = time.AddMinutes(timeSpan); //move up time to next report time
			}

			return View(model);
		}

		[HttpPost]
		public ActionResult ShowFridges(FridgeDTO model)
		{
			if (!String.IsNullOrEmpty(model.DateTimeFromString))
			{
				Session["ChartDataTimeFrom"] = DateTime.ParseExact(model.DateTimeFromString, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture);   
			}
			if (!String.IsNullOrEmpty(model.DateTimeToString))
			{
				Session["ChartDataTimeTo"] = DateTime.ParseExact(model.DateTimeToString, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture);   
			}
			if (model.timeSpan != null && model.timeSpan != 0)
			{
				Session["ChartTimeSpan"] = model.timeSpan;   
			}

			return RedirectToAction("ShowFridges", new { fridgeIdentity = model.FridgeId });
		}

		[HttpPost]
		public ActionResult ShowFridgesList(IEnumerable<FridgeDTO> model)
		{
			return RedirectToAction("ShowFridgesList");
		}

		private ViewResult generateMySqlReport(FridgeDTO model, int timeSpan, DateTime dateFrom, DateTime dateTo)
		{
			var groupReport = _reportService.GenerateGroupReport(model.FridgeId, timeSpan, dateFrom, dateTo);
			var modelLabels = new List<string>();
			var modelValues = new List<int>();

			groupReport.ToList().ForEach(elem => {
				var reportDate = elem.CreatedDate;
				modelLabels.Add(string.Format(@"""{0}""", reportDate.ToString("dd:MM - HH:mm")));
				modelValues.Add(elem.NumberOfCans);
			});

			model.labels = string.Join(",", modelLabels);
			model.values = string.Join(",", modelValues);

			return View(model);
		}
	}
}
