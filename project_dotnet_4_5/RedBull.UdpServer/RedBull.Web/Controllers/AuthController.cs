﻿using Redbull.Authentication.Support;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RedBull.Web.Controllers
{
    public class AuthController : ApiController
    {
        protected void CheckAuth()
        {
            if (!this.IsAminUserLoged())
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Unauthorized" });
        }

        protected string GetUserLogin()
        {
            var userData = Authentication.GetCurrentUserInfo();
            return userData.Login;
        }

        private bool IsAminUserLoged()
        {
            var userData = Authentication.GetCurrentUserInfo();
            if (!String.IsNullOrEmpty(userData.Login) && userData.Login == "Admin") return true;
            return false;
        }
    }
}
