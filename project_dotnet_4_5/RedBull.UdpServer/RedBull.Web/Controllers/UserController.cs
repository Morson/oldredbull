﻿using System.Web.Http;
using Redbull.Authentication.Support;
using Redbull.Authentication.Support.Models.Enums;
using RedBull.Web.Models;

namespace RedBull.Web.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : AuthController
    {
        // POST: api/User/Login
        [Route("Login")]
        public IHttpActionResult Login([FromBody]LoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.UserName.Trim() == "Admin" && model.Password.Trim() == "12345")
            {
                Authentication.LoginUser(model.UserName, UserRoleEnum.Admin, false);
                if (Authentication.GetCurrentUserInfo().Login != null) return Ok();
            }
            return BadRequest("Login lub hasło nieprawidłowe");
        }

        [Route("Logout")]
        public IHttpActionResult GetLogout()
        {
            base.CheckAuth();
            Authentication.LogOutUser();
            return Ok();
        }

        [Route("Data")]
        public string GetData()
        {
            base.CheckAuth();
            return base.GetUserLogin();
        }


    }
}
