﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace RedBull.Web.Core
{
    public static class WebAppConfiguration
    {
        public static bool IsMysqlType()
        {
            return (ConfigurationManager.AppSettings["database:type"] ?? "mssql").ToLower() == "mysql";
        }
    }
}