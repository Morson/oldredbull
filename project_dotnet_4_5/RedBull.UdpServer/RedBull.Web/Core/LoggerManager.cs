﻿using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace RedBull.Web.Core
{
    public class LoggerManager
    {

        public void InitMySql()
        {
            var config = new LoggingConfiguration();

            var databaseTarget = new DatabaseTarget();
            config.AddTarget("database", databaseTarget);

            databaseTarget.KeepConnection = true;
            databaseTarget.DBProvider = "MySql.Data.MySqlClient";
            databaseTarget.ConnectionString = ConnectionString;
            databaseTarget.CommandText = @"insert into log(time_stamp, logger, message, exception, log_level) Values(@TIME_STAMP, @LOGGER, @MESSAGE, @EXCEPTION, @LOGLEVEL)";
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@TIME_STAMP", "${longdate}"));
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@LOGGER", "${logger}"));
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@MESSAGE", "${message}"));
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@EXCEPTION", "${exception:tostring}"));
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@LOGLEVEL", "${level:uppercase=true}"));

            var rule = new LoggingRule("*", LogLevel.Trace, databaseTarget);
            config.LoggingRules.Add(rule);

            LogManager.Configuration = config;
        }

        public void InitSqlServer()
        {
            var config = new LoggingConfiguration();

            var databaseTarget = new DatabaseTarget();
            config.AddTarget("database", databaseTarget);

            databaseTarget.KeepConnection = true;
            databaseTarget.DBProvider = "System.Data.SqlClient";
            databaseTarget.ConnectionString = ConnectionString;
            databaseTarget.CommandText = @"insert into dbo.[log](time_stamp, logger, message, exception, log_level) Values(@TIME_STAMP, @LOGGER, @MESSAGE, @EXCEPTION, @LOGLEVEL)";
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@TIME_STAMP", "${date}"));
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@LOGGER", "${logger}"));
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@MESSAGE", "${message}"));
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@EXCEPTION", "${exception:tostring}"));
            databaseTarget.Parameters.Add(new DatabaseParameterInfo("@LOGLEVEL", "${level:uppercase=true}"));

            var rule = new LoggingRule("*", LogLevel.Trace, databaseTarget);
            config.LoggingRules.Add(rule);

            LogManager.Configuration = config;
        }

        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["RedBull_DB_ConnectionString"].ConnectionString;
            }
        }

    }
}