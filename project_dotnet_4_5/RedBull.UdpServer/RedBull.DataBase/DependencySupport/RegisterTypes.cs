﻿using Microsoft.Practices.Unity;
using RedBull.DataBase._UnitOfWork;
using RedBull.DataBase.Context;
using RedBull.DataBase.Repositories;
using RedBull.DataBase.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using RedBull.DataBase.Repositories.Generic;
using RedBull.Entity.Models;

namespace RedBull.DataBase.DependencySupport
{
    public class RegisterTypes
    {
        private readonly IUnityContainer unityContainer;

        public RegisterTypes(IUnityContainer unityContainer)
        {
            this.unityContainer = unityContainer;
        }

        public IUnityContainer BuildUnityContainer()
        {
            unityContainer.RegisterType<IReportRepository, ReportRepository>();
            unityContainer.RegisterType(typeof(IGenericRepository<>),typeof(GenericRepository<>));

            //context
            unityContainer.RegisterType<IRedBullContext, RedBullContext>(new PerResolveLifetimeManager());

            //UnitOfWork
            unityContainer.RegisterType<IUnitOfWork, UnitOfWork>();

            return unityContainer;
        }
    }
}
