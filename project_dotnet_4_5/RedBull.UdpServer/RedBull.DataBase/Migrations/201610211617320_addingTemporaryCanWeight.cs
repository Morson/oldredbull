namespace RedBull.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingTemporaryCanWeight : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fridges", "TemporaryCanWeight", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fridges", "TemporaryCanWeight");
        }
    }
}
