namespace RedBull.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupViewReport : DbMigration
    {
        public override void Up()
        {
            Sql("if object_id('roundDateTime') is not NULL DROP FUNCTION [roundDateTime]");

            Sql("CREATE FUNCTION[roundDateTime](@p_dateTime datetime, @p_type int) " +
               "RETURNS datetime " +
               "AS " +
               "BEGIN " +
               "DECLARE @ratio int, " +
               "@unit int, " +
               "@counter int, " +
               "@current_unit int, " +
               "@minutes int, " +
               "@p_date datetime, " +
               "@hour_part int; " +
               "SET @hour_part = DATEPART(hour, @p_dateTime); " +
               "SET @minutes = DATEPART(minute, @p_dateTime); " +
               "SET @p_date = @p_dateTime " +
               "SET @ratio = 60 / @p_type; " +
               "SET @unit = 60 / @ratio; " +
               "SET @counter = 0; " +
               "WHILE @counter< @ratio " +
               "BEGIN " +
               "SET @current_unit = @counter * @unit; " +
               "IF @minutes <= @current_unit " +
               "RETURN( convert(varchar(10), @p_date, 120) + ' ' + CONVERT(nvarchar,@hour_part) + ':' + CONVERT(nvarchar, @current_unit) + ':00'); " +
               "SET @counter = @counter + 1; " +
               "END " +
               "SET @hour_part = @hour_part + 1; " +
               "IF @hour_part> 23 " +
               "BEGIN " +
               "SET @hour_part = 0; " +
               "SET @p_date = DATEADD(DAY, 1, @p_date); " +
               "END " +
               "RETURN( convert(varchar(10), @p_date, 120) + ' ' + CONVERT(nvarchar, @hour_part) + ':00:00' ); " +
               "END ");

            Sql("if object_id('roundToDate') is not NULL DROP FUNCTION [roundToDate]");

            Sql("CREATE FUNCTION [roundToDate](@p_dateTime datetime) " +
                "RETURNS datetime " +
                "AS " +
                "BEGIN " +
                "DECLARE @p_date datetime " +
                "SET @p_date = CONVERT(datetime, CONVERT(varchar(10), DATEADD(day, 0, @p_dateTime), 121)) " +
                "RETURN @p_date " +
                "END");

            Sql("if object_id('GroupDayReportView') is not NULL DROP TABLE [GroupDayReportView]");

            Sql("CREATE VIEW GroupDayReportView " +
                "AS " +
                "SELECT " +
                "report.Fridge_Id AS 'FridgeId', " +
                "dbo.roundToDate(report.CreatedDate) AS 'ReportDate', " +
                "AVG(report.NumberOfCans) AS 'AverageNumberOfCans' " +
                "FROM " +
                "NumberOfCanReports AS report " +
                "GROUP BY " +
                "report.Fridge_Id, " +
                "dbo.roundToDate(report.CreatedDate); ");

            Sql("if object_id('GroupReport15View') is not NULL DROP TABLE [GroupReport15View]");

            Sql("CREATE VIEW GroupReport15View " +
                "AS " +
                "SELECT " +
                "report.Fridge_Id AS 'FridgeId', " +
                "dbo.roundDateTime(report.CreatedDate, 15) AS 'ReportDate', " +
                "AVG(report.NumberOfCans) AS 'AverageNumberOfCans' " +
                "FROM " +
                "NumberOfCanReports AS report " +
                "GROUP BY " +
                "report.Fridge_Id, " +
                "dbo.roundDateTime(report.CreatedDate, 15);");

            Sql("if object_id('GroupReport30View') is not NULL DROP TABLE [GroupReport30View]");

            Sql("CREATE VIEW GroupReport30View " +
                "AS " +
                "SELECT " +
                "report.Fridge_Id AS 'FridgeId', " +
                "dbo.roundDateTime(report.CreatedDate, 30) AS 'ReportDate', " +
                "AVG(report.NumberOfCans) AS 'AverageNumberOfCans' " +
                "FROM " +
                "NumberOfCanReports AS report " +
                "GROUP BY " +
                "report.Fridge_Id, " +
                "dbo.roundDateTime(report.CreatedDate, 30);");

            Sql("if object_id('GroupReport60View') is not NULL DROP TABLE [GroupReport60View]");

            Sql("CREATE VIEW GroupReport60View " +
                "AS " +
                "SELECT " +
                "report.Fridge_Id AS 'FridgeId', " +
                "dbo.roundDateTime(report.CreatedDate, 60) AS 'ReportDate', " +
                "AVG(report.NumberOfCans) AS 'AverageNumberOfCans' " +
                "FROM " +
                "NumberOfCanReports AS report " +
                "GROUP BY " +
                "report.Fridge_Id, " +
                "dbo.roundDateTime(report.CreatedDate, 60);");
        }
        
        public override void Down()
        {
        }
    }
    
}
