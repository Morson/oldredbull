namespace RedBull.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class capsamounthistory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CapsAmountHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FridgeRefId = c.Int(nullable: false),
                        ActualAmount = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fridges", t => t.FridgeRefId, cascadeDelete: true)
                .Index(t => t.FridgeRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CapsAmountHistories", "FridgeRefId", "dbo.Fridges");
            DropIndex("dbo.CapsAmountHistories", new[] { "FridgeRefId" });
            DropTable("dbo.CapsAmountHistories");
        }
    }
}
