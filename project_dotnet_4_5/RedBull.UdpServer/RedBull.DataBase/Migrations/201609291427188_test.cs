namespace RedBull.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
           /* RenameTable(name: "dbo.GroupDayReportViews", newName: "GroupDayReportView");
            DropIndex("dbo.Fridges", new[] { "FridgeIdentity" });
            RenameColumn(table: "dbo.NumberOfCanReports", name: "Fridge_Id", newName: "FridgeRefId");
            RenameIndex(table: "dbo.NumberOfCanReports", name: "IX_Fridge_Id", newName: "IX_FridgeRefId");
            CreateTable(
                "dbo.FridgeOwners",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SmsInboxes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SmsText = c.String(),
                        FridgeRefId = c.Int(nullable: false),
                        SmsReciever = c.Int(nullable: false),
                        FridgeOwnerRefId = c.Int(),
                        SuplierRefId = c.Int(),
                        MsgId = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fridges", t => t.FridgeRefId, cascadeDelete: true)
                .ForeignKey("dbo.FridgeOwners", t => t.FridgeOwnerRefId)
                .ForeignKey("dbo.Supliers", t => t.SuplierRefId)
                .Index(t => t.FridgeRefId)
                .Index(t => t.FridgeOwnerRefId)
                .Index(t => t.SuplierRefId);
            
            CreateTable(
                "dbo.Supliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SmsOutboxes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SmeType = c.Int(nullable: false),
                        SmsStatus = c.Int(nullable: false),
                        FridgeRefId = c.Int(nullable: false),
                        SmsReciever = c.Int(nullable: false),
                        FridgeOwnerRefId = c.Int(),
                        SuplierRefId = c.Int(),
                        MsgId = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fridges", t => t.FridgeRefId, cascadeDelete: true)
                .ForeignKey("dbo.FridgeOwners", t => t.FridgeOwnerRefId)
                .ForeignKey("dbo.Supliers", t => t.SuplierRefId)
                .Index(t => t.FridgeRefId)
                .Index(t => t.FridgeOwnerRefId)
                .Index(t => t.SuplierRefId);
            
            CreateTable(
                "dbo.SuplierFridges",
                c => new
                    {
                        Suplier_Id = c.Int(nullable: false),
                        Fridge_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Suplier_Id, t.Fridge_Id })
                .ForeignKey("dbo.Supliers", t => t.Suplier_Id, cascadeDelete: true)
                .ForeignKey("dbo.Fridges", t => t.Fridge_Id, cascadeDelete: true)
                .Index(t => t.Suplier_Id)
                .Index(t => t.Fridge_Id);
            
            AddColumn("dbo.ErrorLogs", "FridgeRefId", c => c.Int());
            AddColumn("dbo.Fridges", "FridgeOwnerRefId", c => c.Int());
            CreateIndex("dbo.ErrorLogs", "FridgeRefId");
            CreateIndex("dbo.Fridges", "FridgeOwnerRefId");
            AddForeignKey("dbo.ErrorLogs", "FridgeRefId", "dbo.Fridges", "Id");
            AddForeignKey("dbo.Fridges", "FridgeOwnerRefId", "dbo.FridgeOwners", "Id");
            DropColumn("dbo.ErrorLogs", "FridgeName");
            DropColumn("dbo.Fridges", "FridgeIdentity");*/
        }
        
        public override void Down()
        {
           /* AddColumn("dbo.Fridges", "FridgeIdentity", c => c.Int(nullable: false));
            AddColumn("dbo.ErrorLogs", "FridgeName", c => c.String());
            DropForeignKey("dbo.SmsOutboxes", "SuplierRefId", "dbo.Supliers");
            DropForeignKey("dbo.SmsOutboxes", "FridgeOwnerRefId", "dbo.FridgeOwners");
            DropForeignKey("dbo.SmsOutboxes", "FridgeRefId", "dbo.Fridges");
            DropForeignKey("dbo.SmsInboxes", "SuplierRefId", "dbo.Supliers");
            DropForeignKey("dbo.SuplierFridges", "Fridge_Id", "dbo.Fridges");
            DropForeignKey("dbo.SuplierFridges", "Suplier_Id", "dbo.Supliers");
            DropForeignKey("dbo.SmsInboxes", "FridgeOwnerRefId", "dbo.FridgeOwners");
            DropForeignKey("dbo.SmsInboxes", "FridgeRefId", "dbo.Fridges");
            DropForeignKey("dbo.Fridges", "FridgeOwnerRefId", "dbo.FridgeOwners");
            DropForeignKey("dbo.ErrorLogs", "FridgeRefId", "dbo.Fridges");
            DropIndex("dbo.SuplierFridges", new[] { "Fridge_Id" });
            DropIndex("dbo.SuplierFridges", new[] { "Suplier_Id" });
            DropIndex("dbo.SmsOutboxes", new[] { "SuplierRefId" });
            DropIndex("dbo.SmsOutboxes", new[] { "FridgeOwnerRefId" });
            DropIndex("dbo.SmsOutboxes", new[] { "FridgeRefId" });
            DropIndex("dbo.SmsInboxes", new[] { "SuplierRefId" });
            DropIndex("dbo.SmsInboxes", new[] { "FridgeOwnerRefId" });
            DropIndex("dbo.SmsInboxes", new[] { "FridgeRefId" });
            DropIndex("dbo.Fridges", new[] { "FridgeOwnerRefId" });
            DropIndex("dbo.ErrorLogs", new[] { "FridgeRefId" });
            DropColumn("dbo.Fridges", "FridgeOwnerRefId");
            DropColumn("dbo.ErrorLogs", "FridgeRefId");
            DropTable("dbo.SuplierFridges");
            DropTable("dbo.SmsOutboxes");
            DropTable("dbo.Supliers");
            DropTable("dbo.SmsInboxes");
            DropTable("dbo.FridgeOwners");
            RenameIndex(table: "dbo.NumberOfCanReports", name: "IX_FridgeRefId", newName: "IX_Fridge_Id");
            RenameColumn(table: "dbo.NumberOfCanReports", name: "FridgeRefId", newName: "Fridge_Id");
            CreateIndex("dbo.Fridges", "FridgeIdentity", unique: true);
            RenameTable(name: "dbo.GroupDayReportView", newName: "GroupDayReportViews");*/
        }
    }
}
