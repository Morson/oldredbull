namespace RedBull.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ErrorLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ErrorText = c.String(),
                        FridgeName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Fridges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FridgeIdentity = c.Int(nullable: false),
                        LastActivity = c.DateTime(nullable: false),
                        FridgeError = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Capacity = c.Int(nullable: false),
                        SallesAvaragePerDay = c.Int(nullable: false),
                        IsEmpty = c.Boolean(nullable: false),
                        FridgeDescription = c.String(),
                        ClientName = c.String(),
                        FridgeManagerName = c.String(),
                        PhoneNumber = c.String(),
                        EmailAddress = c.String(),
                        Address = c.String(),
                        GoogleLocalization = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.FridgeIdentity, unique: true);

            
            CreateTable(
                "dbo.log",
                c => new
                    {
                        pkid = c.Long(nullable: false, identity: true),
                        time_stamp = c.DateTime(nullable: false),
                        logger = c.String(maxLength: 255),
                        message = c.String(),
                        exception = c.String(),
                        log_level = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.pkid);
            
            CreateTable(
                "dbo.NumberOfCanReports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NumberOfCans = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        Fridge_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fridges", t => t.Fridge_Id, cascadeDelete: true)
                .Index(t => t.Fridge_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NumberOfCanReports", "Fridge_Id", "dbo.Fridges");
            DropIndex("dbo.NumberOfCanReports", new[] { "Fridge_Id" });
            DropIndex("dbo.Fridges", new[] { "FridgeIdentity" });
            DropTable("dbo.NumberOfCanReports");
            DropTable("dbo.log");
            DropTable("dbo.Fridges");
            DropTable("dbo.ErrorLogs");
        }
    }
}
