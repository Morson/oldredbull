namespace RedBull.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FridgeOwnWeight : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fridges", "FridgeWeight", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fridges", "FridgeWeight");
        }
    }
}
