namespace RedBull.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fridgeOwnerExtension : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Fridges", "FridgeOwnerRefId", "dbo.FridgeOwners");
            DropIndex("dbo.Fridges", new[] { "FridgeOwnerRefId" });
            CreateTable(
                "dbo.FridgeOwnerFridges",
                c => new
                    {
                        FridgeOwner_Id = c.Int(nullable: false),
                        Fridge_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FridgeOwner_Id, t.Fridge_Id })
                .ForeignKey("dbo.FridgeOwners", t => t.FridgeOwner_Id, cascadeDelete: true)
                .ForeignKey("dbo.Fridges", t => t.Fridge_Id, cascadeDelete: true)
                .Index(t => t.FridgeOwner_Id)
                .Index(t => t.Fridge_Id);
            
            DropColumn("dbo.Fridges", "FridgeOwnerRefId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Fridges", "FridgeOwnerRefId", c => c.Int());
            DropForeignKey("dbo.FridgeOwnerFridges", "Fridge_Id", "dbo.Fridges");
            DropForeignKey("dbo.FridgeOwnerFridges", "FridgeOwner_Id", "dbo.FridgeOwners");
            DropIndex("dbo.FridgeOwnerFridges", new[] { "Fridge_Id" });
            DropIndex("dbo.FridgeOwnerFridges", new[] { "FridgeOwner_Id" });
            DropTable("dbo.FridgeOwnerFridges");
            CreateIndex("dbo.Fridges", "FridgeOwnerRefId");
            AddForeignKey("dbo.Fridges", "FridgeOwnerRefId", "dbo.FridgeOwners", "Id");
        }
    }
}
