namespace RedBull.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedWeight : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NumberOfCanReports", "TemporaryWeight", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NumberOfCanReports", "TemporaryWeight");
        }
    }
}
