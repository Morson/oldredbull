﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RedBull.DataBase._UnitOfWork
{
    public interface IUnitOfWork
    {
        int Commit();
        void Dispose();
    }
}
