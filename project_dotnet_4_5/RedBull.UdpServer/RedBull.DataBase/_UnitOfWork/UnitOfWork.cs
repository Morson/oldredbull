﻿using RedBull.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace RedBull.DataBase._UnitOfWork
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private IRedBullContext _dbContext;

        public UnitOfWork(IRedBullContext context)
        {
            _dbContext = context;
        }

        public int Commit()
        {
            return _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
                _dbContext = null;
            }
        }
    }
}
