﻿using RedBull.Entity.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace RedBull.DataBase.Repositories.Generic
{
    public interface IGenericRepository<T> where T : class 
    {
        T GetById(object id);
        IQueryable<T> GetAll();
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        T Add(T entity);
        T Delete(T entity);
        void Edit(T entity);
        void Save();
    }
}
