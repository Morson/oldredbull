﻿using RedBull.DataBase.Context;
using RedBull.Entity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace RedBull.DataBase.Repositories.Generic
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class 
    {
        protected readonly IRedBullContext _entities;
        protected readonly IDbSet<T> _dbset;

        public GenericRepository(IRedBullContext context)
        {
            _entities = context;
            _dbset = context.Set<T>();
        }

        public virtual T GetById(object id)
        {
            return _dbset.Find(id);
        }

        public virtual IQueryable<T> GetAll()
        {
            return _dbset.AsQueryable<T>();
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicete)
        {
            return _dbset.Where(predicete).AsQueryable();
        }

        public virtual T Add(T entity)
        {
            return _dbset.Add(entity);
        }

        public virtual T Delete(T entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            _entities.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }
    }
}
