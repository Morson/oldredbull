﻿using RedBull.DataBase.Repositories.Generic;
using RedBull.Entity.Models;
using RedBull.Entity.Report;
using System;
using System.Collections.Generic;

namespace RedBull.DataBase.Repositories.Interfaces
{
    public interface IReportRepository : IGenericRepository<NumberOfCanReport>
    {

        IEnumerable<NumberOfCanReport> GenerateGroupReport(long fridgeId, int type, DateTime fromDate, DateTime toDate);

    }
}
