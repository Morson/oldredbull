﻿using RedBull.DataBase.Repositories.Generic;
using RedBull.Entity.Models;
using RedBull.DataBase.Context;
using RedBull.DataBase.Repositories.Interfaces;
using RedBull.Entity.Report;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Entity;

namespace RedBull.DataBase.Repositories
{
    public class ReportRepository : GenericRepository<NumberOfCanReport>, IReportRepository
    {
        public ReportRepository(IRedBullContext _context)
            : base(_context)
        {

        }

        /// <summary>
        /// Improvement only for MySQL
        /// </summary>
        /// <param name="fridgeId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<NumberOfCanReport> GenerateGroupReport(long fridgeId, int type, DateTime fromDate, DateTime toDate)
        {
            //juz nie korzystam z widokow , bo mam nowa koncepcje na filtrowanie rekordow: wyswietlam tylko
            //takie rekordy, w ktorych ilosc puszek sie zmienila w stosunku do poprzedniego rekordu.
            var reports =
                _entities.NumberOfCanReports.Where(
                        noc => noc.FridgeRefId == fridgeId && noc.CreatedDate >= fromDate && noc.CreatedDate <= toDate)
                    .ToList();

            var reports2 = new List<NumberOfCanReport>();
            var candidate = reports[0];
            for (int i = 1; i < reports.Count; i++)
            {
                if (reports[i].NumberOfCans==candidate.NumberOfCans)
                {
                    candidate = reports[i];
                }
                else
                {
                    reports2.Add(candidate);
                    candidate = reports[i];
                }
            }
            return reports2;
        }


    }
}
