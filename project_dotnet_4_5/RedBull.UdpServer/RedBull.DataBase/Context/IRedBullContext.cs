﻿using RedBull.Entity.Report;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using RedBull.Entity.Models;
using RedBull.Entity.Models.Logs;

namespace RedBull.DataBase.Context
{
    public interface IRedBullContext : IDisposable
    {
        DbSet<Fridge> Fridges { get; set; }
        DbSet<NumberOfCanReport> NumberOfCanReports { get; set; }
        DbSet<ErrorLog> ErrorLogs { get; set; }
        DbSet<Log> Logs { get; set; }
        DbSet<FridgeOwner> FridgeOwners { get; set; }
        DbSet<Suplier> Supliers { get; set; }
        DbSet<SmsInbox> SmsInboxes { get; set; }
        DbSet<SmsOutbox> SmsOutboxes { get; set; }

        DbSet<GroupDayReportView> GroupDayReport { get; set; }
        DbSet<GroupReport15View> GroupReport15 { get; set; }
        DbSet<GroupReport30View> GroupReport30 { get; set; }
        DbSet<GroupReport60View> GroupReport60 { get; set; }
        DbSet<CapsAmountHistory> CapsAmountHistories { get; set; }
        DbSet<T> Set<T>() where T : class;
        DbEntityEntry Entry(object entity);
        int SaveChanges();

    }
}
