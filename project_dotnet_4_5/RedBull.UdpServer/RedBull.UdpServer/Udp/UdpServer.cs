﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using RedBull.Common;

namespace RedBull.UdpServer.Udp
{
    public class UdpServer
    {
        private Socket mSocket;
        private SocketAsyncEventArgs mRxArgs, mTxArgs;
        private IPEndPoint mAnyEndPoint;
        private IPEndPoint mLocalEndPoint;

        private readonly Func<string, UdpResult> processResponseFunc;
        private readonly int port;
        private readonly ILogger logger;

        public UdpServer(int port, Func<string, UdpResult> processResponseFunc, ILogger logger)
        {
            this.port = port;
            this.processResponseFunc = processResponseFunc;
            this.logger = logger;
        }

        public void Start()
        {
            mAnyEndPoint = new IPEndPoint(IPAddress.Any, 0);
            mLocalEndPoint = new IPEndPoint(IPAddress.Any, port);

            mRxArgs = new SocketAsyncEventArgs();
            mTxArgs = new SocketAsyncEventArgs();

            mRxArgs.Completed += ReceiveComplete;
            mTxArgs.Completed += SendComplete;

            mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

#if WINDOWS           
            const int SIO_UDP_CONNRESET = -1744830452;
            byte[] inValue = new byte[] { 0 };
            byte[] outValue = new byte[] { 0 };
            mSocket.IOControl(SIO_UDP_CONNRESET, inValue, outValue);
#endif          

            mSocket.Bind(mLocalEndPoint);
            ReceiveNext();
        }

        private void ReceiveNext()
        {
            logger.Log("Receive");

            var buffer = new byte[1024];

            mRxArgs.RemoteEndPoint = mAnyEndPoint;
            mRxArgs.SetBuffer(buffer, 0, buffer.Length);

            if (!mSocket.ReceiveFromAsync(mRxArgs))
                logger.Log("Error in ReceiveNext: " + mRxArgs.SocketError);
        }

        private void ReceiveComplete(object sender, SocketAsyncEventArgs e)
        {
            logger.Log("Receive Complete: " + mRxArgs.SocketError);

            if (mRxArgs.SocketError != SocketError.Success)
            {
                ReceiveNext();
                return;
            }

            var data = Encoding.ASCII.GetString(mRxArgs.Buffer, 0, mRxArgs.BytesTransferred);
            logger.Log("Data received: " + data);

            var result = processResponseFunc.Invoke(data);

            var resultJson = JsonConvert.SerializeObject(result);
            var buffer = Encoding.ASCII.GetBytes(resultJson);

            mTxArgs.SetBuffer(buffer, 0, buffer.Length);
            mTxArgs.RemoteEndPoint = mRxArgs.RemoteEndPoint;

            logger.Log("Sending reply packet");

            if (!mSocket.SendToAsync(mTxArgs))
            {
                logger.Log("Error in ReceiveComplete: " + mRxArgs.SocketError);
                ReceiveNext();
                return;
            }
        }

        private void SendComplete(object sender, SocketAsyncEventArgs e)
        {
            logger.Log("Send Complete: " + mTxArgs.SocketError);

            if (mTxArgs.SocketError != SocketError.Success)
            {
                ReceiveNext();
                return;
            }

            ReceiveNext();
        }
    }
}
