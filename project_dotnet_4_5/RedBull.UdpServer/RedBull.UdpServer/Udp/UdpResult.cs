﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.UdpServer.Udp
{
    public class UdpResult
    {
        public UdpResultStatus Status { get; set; }

        public UdpResult(UdpResultStatus status)
        {
            Status = status;
        }
    }
}
