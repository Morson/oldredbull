﻿using NLog;
using RedBull.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace RedBull.Mailing
{
    public class Mail
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public bool SentMessage(string emailAddress, EmailTypesEnum emailType, Fridge fridge, string numberOfCans, string error)
        {
            List<MessagesTypes> messages = InitialiseMessages();

            MailMessage mail = new MailMessage("RedBullAirfridge@havasww.com.pl", emailAddress);

            MessagesTypes emailTemplate;
            if (emailType == EmailTypesEnum.EmptyLogMail)
            {
                emailTemplate = messages.Where(a => a.emailType == EmailTypesEnum.EmptyLogMail).First();
            }
            else
            {
                emailTemplate = messages.Where(a => a.emailType == EmailTypesEnum.ErrorMail).First();
            }

            mail.ReplyTo = new MailAddress("RedBullAirfridge@havasww.com.pl");
            mail.Subject = String.Format(emailTemplate.Title.ToString(), fridge.ClientName, fridge.Address);
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;

            StringBuilder sb = new StringBuilder();

            if (emailType == EmailTypesEnum.EmptyLogMail)
            {
                mail.Body = String.Format(emailTemplate.Body.ToString(), fridge.Id.ToString(), fridge.ClientName, fridge.Address, fridge.PhoneNumber, fridge.EmailAddress, numberOfCans, fridge.Capacity);
            }
            else if (emailType == EmailTypesEnum.ErrorMail)
            {
                mail.Body = String.Format(emailTemplate.Body.ToString(), fridge.Id.ToString(), fridge.ClientName, fridge.Address, fridge.PhoneNumber, fridge.EmailAddress, error);
            }

            SmtpClient client = new SmtpConfigReader().CreateSmtpClient();

            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return true;
        }

        public List<MessagesTypes> InitialiseMessages()
        {
            List<MessagesTypes> result = new List<MessagesTypes>();

            result.Add(new MessagesTypes(){ 
                Body = @"
                <div style=""background-color: #DADADA; width: 747px; margin-left: auto; margin-right: auto; font-family: Calibri;"">
	                <table cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" width=""747"">
                        <tr>
			                <td colspan=""3"">
				                <table cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" width=""747"">
					                <tr>
						                <td width=""350"" height=""70"">
							               <img style=""display:block; padding: 50px;"" src=""http://4everstatic.com/bilder/110x82/logos/red-bull-157114.jpg"">
						                </td>
						                <td height=""170"" align=""left"" valign=""top"">
							                <br>
							                <font style=""font-size: 18px; line-height: 26px"" size=""4""><font color=""#323232"">Nazwa lodówki:</font><font color=""#15793d"">{0}</font><br>
							                <font style=""font-size: 18px; line-height: 26px"" size=""4""><font color=""#323232"">Nazwa lokalu:</font><font color=""#15793d"">{1}</font><br>
							                <font style=""font-size: 18px; line-height: 26px"" size=""4""><font color=""#323232"">Adres lokalu:</font><font color=""#15793d"">{2}</font><br>
							                <font style=""font-size: 18px; line-height: 26px"" size=""4""><font color=""#323232"">Numer do właściciela:</font><font color=""#15793d"">{3}</font><br>
							                <font color=""#323232"">e-mail właściciela:</font><font color=""#15793d"">{4}</font></font>
						                </td>
						                <td width=""20"" height=""170"">
						                </td>
					                </tr>
				                </table>
			                </td>
		                </tr>
		                <tr>
			                <td colspan=""3"" width=""747"" height=""79""></td>
		                </tr>
		                <tr>
			                <td width=""29"">
			                </td>
			                <td style=""background: #ffffff; font-size:13px; text-align: left"" align=""left"" width=""747"">
			
				                <p style=""padding-left: 20px; padding-right: 20px; color: #004080; font-size: 16px;"">
					                <strong>Witamy, </strong>
					                <br />
					                <br />
					                <strong>Uwaga</strong> - Ilość puszek w lodówce wynosi <strong>{5}</strong>
					                <br />
					                <br />
					                Przypominamy, że pojemność lodówki wynosi <strong>{6} puszek</strong>.
				                </p>
				
				                <p style=""padding-left: 20px; padding-right: 20px; color: #004080; font-size: 16px;"">
					                Pozdrawiamy i życzymy miłego dnia,
					                <br>
					                <strong>Zespół Red Bull</strong>.
				                </p>
			                </td>
			                <td width=""32"">
			                </td>
		                    </tr>
		                    <tr>
			                    <td colspan=""3"" width=""747"" height=""79""></td>
		                    </tr>
	                </table>
                </div>",
                emailType = EmailTypesEnum.EmptyLogMail,
                Title = "W lodówce {0} zlokalizowanej przy {1} zaczyna brakować Red Bulla"
            });

            result.Add(new MessagesTypes()
            {
                Body = @"
                <div style=""background-color: #DADADA; width: 747px; margin-left: auto; margin-right: auto; font-family: Calibri;"">
	                <table cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" width=""747"">
                        <tr>
			                <td colspan=""3"">
				                <table cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" width=""747"">
					                <tr>
						                <td width=""350"" height=""70"">
							                <img style=""display:block; padding: 50px;"" src=""http://4everstatic.com/bilder/110x82/logos/red-bull-157114.jpg"">
						                </td>
						                <td height=""170"" align=""left"" valign=""top"">
							                <br>
							                <font style=""font-size: 18px; line-height: 26px"" size=""4""><font color=""#323232"">Nazwa lodówki:</font><font color=""#15793d"">{0}</font><br>
							                <font style=""font-size: 18px; line-height: 26px"" size=""4""><font color=""#323232"">Nazwa lokalu:</font><font color=""#15793d"">{1}</font><br>
							                <font style=""font-size: 18px; line-height: 26px"" size=""4""><font color=""#323232"">Adres lokalu:</font><font color=""#15793d"">{2}</font><br>
							                <font style=""font-size: 18px; line-height: 26px"" size=""4""><font color=""#323232"">Numer właściciela:</font><font color=""#15793d"">{3}</font><br>
							                <font color=""#323232"">e-mail właściciela:</font><font color=""#15793d"">{4}</font></font>
						                </td>
						                <td width=""20"" height=""170"">
						                </td>
					                </tr>
				                </table>
			                </td>
		                </tr>
		                <tr>
			                <td colspan=""3"" width=""747"" height=""79""></td>
		                </tr>
		                <tr>
			                <td width=""29"">
			                </td>
			                <td style=""background: #ffffff; font-size:13px; text-align: left"" align=""left"" width=""747"">
			
				                <p style=""padding-left: 20px; padding-right: 20px; color: #004080; font-size: 16px;"">
					                <strong>Witamy, </strong>
					                <br />
					                <br />
					                <strong>Uwaga</strong> - w lodówce wystąpił następujący błąd:
					                <br />
					                <br />
					                <strong>{5}</strong>
				                </p>
				
				                <p style=""padding-left: 20px; padding-right: 20px; color: #004080; font-size: 16px;"">
					                Pozdrawiamy i życzymy miłego dnia,
					                <br>
					                <strong>Zespół Red Bull</strong>.
				                </p>
			                </td>
			                <td width=""32"">
			                </td>
		                    </tr>
		                    <tr>
			                    <td colspan=""3"" width=""747"" height=""79""></td>
		                    </tr>
	                </table>
                </div>",
                emailType = EmailTypesEnum.ErrorMail,
                Title = "Błąd - Lodówka w {0} ({1})"
            });

            return result;
        }
    }
}


