﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RedBull.Mailing
{
    public class MessagesTypes
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public EmailTypesEnum emailType { get; set; }
    }
}
