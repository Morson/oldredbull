﻿using Microsoft.Practices.Unity;
using RedBull.Common;
using RedBull.Domain.Services;
using RedBull.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Domain.DependencySupport
{
    public class RegisterTypes
    {
        private readonly IUnityContainer unityContainer;

        public RegisterTypes(IUnityContainer unityContainer)
        {
            this.unityContainer = unityContainer;
        }

        public IUnityContainer BuildUnityContainer(bool mysqlMode)
        {
            if (mysqlMode)
            {
                new Redbull.DAL.MySql.DependencySupport.RegisterTypes(unityContainer).BuildUnityContainer();
            }
            else
            {
                new RedBull.DataBase.DependencySupport.RegisterTypes(unityContainer).BuildUnityContainer();
            }

            //Services
            unityContainer.RegisterType<IErrorServices, ErrorServices>();
            unityContainer.RegisterType<IFridgeService, FridgeService>();
            unityContainer.RegisterType<IReportService, ReportService>();
            unityContainer.RegisterType<ISmsInboxService, SmsInboxService>();
            unityContainer.RegisterType<ISmsOutboxService, SmsOutboxService>();
            unityContainer.RegisterType<ILogger, Logger>();

            return unityContainer;
        }
    }
}
