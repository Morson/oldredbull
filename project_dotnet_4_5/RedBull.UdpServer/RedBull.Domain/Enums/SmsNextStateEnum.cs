﻿namespace RedBull.Domain.Enums
{
    public enum SmsNextStateEnum
    {
        SendRequestToSuplier,
        SendInfoToFridgeOwner,
        RequestToSuplierAlreadySended,
        SendSuplierToLateInfo
    }
}