﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Domain.Models
{
    public class Sms
    {
        public string PhoneNumber { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
    }
}
