﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Domain.Models
{
    public class ChartReportModel
    {
        public DateTime x { get; set; }
        public int y { get; set; }  
    }
}
