﻿using RedBull.DataBase._UnitOfWork;
using RedBull.DataBase.Repositories.Interfaces;
using RedBull.Domain.Services.Generic;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedBull.DataBase.Repositories.Generic;

namespace RedBull.Domain.Services
{
    public class FridgeService : GenericService<Fridge>, IFridgeService
    {
        private readonly IGenericRepository<Fridge> _fridgeRepositoryy;

        public FridgeService(IUnitOfWork unitOfWork,
            IGenericRepository<Fridge> _fridgeRepositoryy)
            : base(unitOfWork, _fridgeRepositoryy)
        {
            this._fridgeRepositoryy = _fridgeRepositoryy;
        }

        public Fridge GetFridgeById(int fridgeId)
        {
            return _fridgeRepositoryy.GetById(fridgeId);
        }

        public IEnumerable<Fridge> GetAlmostEmptyFridges()
        {
            return
                _fridgeRepositoryy.FindBy(
                    fr => fr.NumberOfCanReports.OrderByDescending(num => num.Id).FirstOrDefault().NumberOfCans < 5);
        }
    }
}
