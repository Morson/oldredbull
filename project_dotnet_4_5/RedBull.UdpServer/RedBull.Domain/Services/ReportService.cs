﻿using RedBull.DataBase._UnitOfWork;
using RedBull.DataBase.Repositories.Interfaces;
using RedBull.Domain.Services.Generic;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models;
using RedBull.Entity.Report;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedBull.DataBase.Repositories.Generic;
using RedBull.Domain.Models;

namespace RedBull.Domain.Services
{
    public class ReportService : GenericService<NumberOfCanReport>, IReportService
    {
        
        private readonly IGenericRepository<NumberOfCanReport> _numberOfCanRepository;

        public ReportService(IUnitOfWork unitOfWork,IGenericRepository<NumberOfCanReport> _numberOfCanRepository)
            : base(unitOfWork, _numberOfCanRepository)
        {
            this._numberOfCanRepository = _numberOfCanRepository;
        }

        public int? GetNumberOdCans(int fridgeIdentity)
        {
            return _numberOfCanRepository.FindBy(a => a.Fridge.Id == fridgeIdentity).OrderByDescending(f=>f.Id).FirstOrDefault().NumberOfCans;
        }

        public int GetLastTwooWeekNumberOfCansAvarage(int fridgeId)
        {
            DateTime dateFrom = DateTime.Now.AddDays(-14);

            var avarage= _numberOfCanRepository
                .FindBy(
                        a => 
                        a.CreatedDate >= dateFrom && 
                        a.CreatedDate <= DateTime.Now && 
                        a.Fridge.Id == fridgeId)
                    .Average(c => c.NumberOfCans);

            return (int)avarage;
        }

        public int? GetNumberOdCans(DateTime fromTime, int fridgeIdentity)
        {
            return _numberOfCanRepository.FindBy(a => a.Fridge.Id == fridgeIdentity).OrderBy(c => c.CreatedDate).FirstOrDefault().NumberOfCans;
            
        }

        public IEnumerable<NumberOfCanReport> GenerateGroupReport(int fridgeId, int type, DateTime fromDate,
            DateTime toDate)
        {
            var pierwsze = _numberOfCanRepository.FindBy(
                report =>
                    report.FridgeRefId == fridgeId &&
                    report.CreatedDate >= fromDate &&
                    report.CreatedDate <= toDate);

            var dwa = pierwsze.AsEnumerable();

            var join = pierwsze.Join(dwa,
                outer => outer.Id + 1,
                inner => inner.Id,
                (inner, outer) => new { inner, outer });

            var filterJoin = join.Where(rep => rep.inner.NumberOfCans != rep.outer.NumberOfCans);

            var selectionedFilteredJoin = filterJoin.Select(r => r.outer).AsEnumerable();

            return selectionedFilteredJoin;
        }

        public IEnumerable<NumberOfCanReport> GenerateGroupReport(int fridgeId, DateTime dateFrom ,DateTime dateTo)
        {
            var filteredReports = _numberOfCanRepository.FindBy(
                report =>
                    report.FridgeRefId == fridgeId &&
                    report.CreatedDate >= dateFrom &&
                    report.CreatedDate <= dateTo).ToList();

            var changedOnlyList = new List<NumberOfCanReport>();
            var last = filteredReports.Count;

            if (last>0)
            {
                changedOnlyList.Add(filteredReports[0]);
                for (int i = 1; i < last - 1; i++)
                {
                    if (Math.Abs(filteredReports[i].NumberOfCans - changedOnlyList.Last().NumberOfCans) > 1)
                        changedOnlyList.Add(filteredReports[i]);
                }
                changedOnlyList.Add(filteredReports[last - 1]);
            }
                        
            return changedOnlyList;
        }

        public IEnumerable<ChartReportModel> GetChartReports(int fridgeId, int type, DateTime fromDate, DateTime toDate)
        {
            var result = this.GenerateGroupReport(fridgeId,type,fromDate,toDate);
            var reports = new List<ChartReportModel>();

            result.ToList().ForEach(p =>
            {
                var tempReport = new ChartReportModel()
                {
                    x = p.CreatedDate,
                    y = p.NumberOfCans
                };

                reports.Add(tempReport);
            });

            return reports;
        }

        public void MakeReport(Fridge fridge, int numberOfCans, int weight)
        {
            _numberOfCanRepository.Add(new NumberOfCanReport() {Fridge = fridge, NumberOfCans = numberOfCans,TemporaryWeight = weight});
        }

        public IQueryable<NumberOfCanReport> filterReports(int fridgeId, DateTime dateFrom, DateTime dateTo)
        {
            return 
                _numberOfCanRepository.FindBy(
                    report =>
                        report.FridgeRefId == fridgeId &&
                        report.CreatedDate >= dateFrom &&
                        report.CreatedDate <= dateTo);
        }

        public NumberOfCanReport GetLastReport(int fridgeId)
        {
            return _numberOfCanRepository.FindBy(f => f.Fridge.Id == fridgeId).OrderByDescending(n => n.Id).FirstOrDefault();
        }
    }
}
