﻿using RedBull.Domain.Services.Generic;
using RedBull.Entity.Models;
using RedBull.Entity.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Domain.Services.Interfaces
{
    public interface IReportService : IGenericService<NumberOfCanReport>
    {
        int? GetNumberOdCans(int fridgeIdentity);
        int? GetNumberOdCans(DateTime fromTime, int fridgeIdentity);
        IEnumerable<NumberOfCanReport> GenerateGroupReport(int fridgeId, int type, DateTime fromDate, DateTime toDate);
        IEnumerable<NumberOfCanReport> GenerateGroupReport(int fridgeId, DateTime dateFrom, DateTime dateTo);
        int GetLastTwooWeekNumberOfCansAvarage(int fridgeId);

        void MakeReport(Fridge fridge, int numberOfCans, int weight = 123);
        NumberOfCanReport GetLastReport(int fridgeId);
    }
}
