﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedBull.Domain.Enums;
using RedBull.Domain.Services.Generic;
using RedBull.Entity.Models;

namespace RedBull.Domain.Services.Interfaces
{
    public interface ISmsInboxService:IGenericService<SmsInbox>
    {
        SmsNextStateEnum? CreateSms(string msgId, string sms_text);
    }
}
