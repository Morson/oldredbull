﻿using RedBull.Domain.Enums;
using RedBull.Domain.Services.Generic;
using RedBull.Entity.Models;

namespace RedBull.Domain.Services.Interfaces
{
    public interface ISmsOutboxService:IGenericService<SmsOutbox>
    {
        void UpdateStatus(string msgId, string status);
        void SendSms(Fridge fridge);
        void SendSms(SmsNextStateEnum nextState,string msgId);
    }
}