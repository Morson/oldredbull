﻿using RedBull.Domain.Services.Generic;
using RedBull.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Domain.Services.Interfaces
{
    public interface IFridgeService : IGenericService<Fridge>
    {
        Fridge GetFridgeById(int fridgeId);
        IEnumerable<Fridge> GetAlmostEmptyFridges();
    }
}
