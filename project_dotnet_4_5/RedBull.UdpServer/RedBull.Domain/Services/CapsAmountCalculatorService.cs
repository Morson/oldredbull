﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedBull.DataBase.Repositories.Generic;
using RedBull.DataBase._UnitOfWork;
using RedBull.Domain.Services.Generic;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models;

namespace RedBull.Domain.Services
{
    public class CapsAmountCalculatorService:GenericService<CapsAmountHistory>,ICapsAmountCalculatorService
    {
        private readonly IGenericRepository<CapsAmountHistory> _capsAmountRepository;
        private readonly IGenericRepository<NumberOfCanReport> _numberOfCanRepository;
        private readonly IGenericRepository<Fridge> _fridgeRepository;
        public CapsAmountCalculatorService(IUnitOfWork unitOfWork,
            IGenericRepository<CapsAmountHistory> _capsAmountRepository,
            IGenericRepository<NumberOfCanReport> _numberOfCanRepository,
            IGenericRepository<Fridge> _fridgeRepository ) :
            base(unitOfWork, _capsAmountRepository)
        {
            this._capsAmountRepository = _capsAmountRepository;
            this._numberOfCanRepository = _numberOfCanRepository;
            this._fridgeRepository = _fridgeRepository;
        }

        public bool ActualizeServerWeightBase(int fridgeId, int actualWeight)
        {
            //jezeli cos sie spierdoli - wedlug dobrych praktyk - nalezy zwracac falsa
            //if(zjebalosie)
            //{
            //    return false;
            //}
            Fridge fridge = _fridgeRepository.GetById(fridgeId);
            _capsAmountRepository.Add(new CapsAmountHistory() {ActualAmount = actualWeight, Fridge = fridge});
            return true;
            
        }
        public void MakeReport(Fridge fridge, int numberOfCans, int weight)
        {
            _numberOfCanRepository.Add(new NumberOfCanReport() { Fridge = fridge, NumberOfCans = numberOfCans, TemporaryWeight = weight });
        }
        private NumberOfCanReport GetLastCanReport(int fridgeId)
        {

            return _numberOfCanRepository.FindBy(
                    num => num.FridgeRefId == fridgeId).OrderByDescending(num => num.Id)
                .FirstOrDefault();
        }

        private bool IsWeitghtDecalibration(int fridgeId, int actualWeight)
        {
            int criticalValue = 50;//ustalic
            NumberOfCanReport numberOfCanReport = GetLastCanReport(fridgeId);
            int oldWeight = numberOfCanReport.TemporaryWeight;
            int weightDifference = Math.Abs(oldWeight - actualWeight);
            if (weightDifference>=criticalValue)
            {
                return true;
            }
            return false;
        }

        public void CalculateWeight(Fridge fridge, int cans, int actualWeight)
        {
            if (IsWeitghtDecalibration(fridge.Id,actualWeight))
            {
                
            }
        }

        // metoda do mielenia wagi
    }
}
