﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using RedBull.DataBase.Repositories.Generic;
using RedBull.DataBase.Repositories.Interfaces;
using RedBull.DataBase._UnitOfWork;
using RedBull.Domain.Enums;
using RedBull.Domain.Models;
using RedBull.Domain.Services.Generic;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models;
using RedBull.Entity.Models.Enums;
using SMSApi.Api;

namespace RedBull.Domain.Services
{
    public class SmsOutboxService : GenericService<SmsOutbox>, ISmsOutboxService
    {
        private readonly IGenericRepository<SmsOutbox> _smsOutboxRepository;
        private readonly IGenericRepository<Fridge> _fridgeRepositoryy;

        private Client client;
        private SMSFactory smsFactory;

        public SmsOutboxService(IUnitOfWork unitOfWork, IGenericRepository<SmsOutbox> _smsOutboxRepository,
            IGenericRepository<Fridge> _fridgeRepositoryy)
            : base(unitOfWork, _smsOutboxRepository)
        {

            this._smsOutboxRepository = _smsOutboxRepository;
            this._fridgeRepositoryy = _fridgeRepositoryy;

            client = new Client("morsonio");
            client.SetPasswordHash("7d451a480be561890944e1f449f91b30");
            smsFactory = new SMSFactory(client);
        }

        

        public void UpdateStatus(string msgId, string status)
        {
            //rozdzielenie stringa raportow na liste stringow
            var msgIdList = msgId.Split(',').ToList<string>();
            var statusList = status.Split(',').ToList<string>();
            //sprawdzenie czy dla kazdego raportu doszedl status
            if (msgIdList.Count==statusList.Count)
                for (int i = 0; i < msgIdList.Count; i++)
                {
                    //sprawdzenie czy status jest prawidłowy
                    if(statusList[i]=="404")
                        if(IsSmsExist(msgIdList[i]))
                            UpdateStatusByMsgId(msgId);
                }
        }

        public bool IsSmsExist(string msgId)
        {
            return _smsOutboxRepository.FindBy(sms => sms.MsgId == msgId) != null;

        }

        public void UpdateStatusByMsgId(string msgId)
        {
            _smsOutboxRepository.FindBy(sms=>sms.MsgId==msgId).First().SmsStatus=SmsStatusEnum.Recieved;
            _smsOutboxRepository.Save();
        }

        public bool IsSended(int fridgeId, int fridgeOwnerId)
        {
            var smsSendedToday =
                _smsOutboxRepository.FindBy(sm => sm.FridgeRefId == fridgeId && sm.FridgeOwnerRefId == fridgeOwnerId)
                    .OrderByDescending(sm => sm.CreatedDate)
                    .FirstOrDefault();
            if (smsSendedToday != null)
            {
                return smsSendedToday.CreatedDate.Date == DateTime.Today.Date;
            }

            return false;

        }

        

        public void SendSms(Fridge fridge)

        {
            var fridgeId = fridge.Id;
            var fridgeOwners = fridge.FridgeOwners.ToList();
           // var fridgeOwners2 = _fridgeRepositoryy.FindBy(f=>f.Id==fridgeId).Include(fr=>fr.FridgeOwners)
            
            for (int i = 0; i < fridgeOwners.Count; i++)
            {
                var ownerId = fridgeOwners[i].Id;
                if (!IsSended(fridge.Id, ownerId))
                {
                    var type = SmsTypeEnum.TwoWayRequest;
                    var sms = new Sms()
                    {
                        PhoneNumber = fridgeOwners[i].PhoneNumber,
                        Type = type.Encode(),
                        Text =
                            string.Format(
                                "Mała ilość puszek RedBull w lodówce: {0}. Jeśli chcesz zamówić dostawę odpowiedz TAK.",
                                fridge.FridgeDescription)
                    };


                    var mainMsgId = Send(sms);

                    base.Create(new SmsOutbox()
                    {
                        Fridge = fridge,
                        CreatedDate = DateTime.Now,
                        SmeType = type,
                        FridgeOwner = fridgeOwners[i],
                        MsgId = mainMsgId,
                        SmsReciever = SmsRecieverEnum.Owner,
                        SmsStatus = SmsStatusEnum.NotRecieved,
                    });
                }
            }

        }

        public void SendSms(SmsNextStateEnum nextState, string msgId)
        {
            var lastSms = _smsOutboxRepository.FindBy(sm => sm.MsgId == msgId).FirstOrDefault();
            if (lastSms != null)
            {


                var fridge = lastSms.Fridge;


                SmsTypeEnum smsType;
                SmsRecieverEnum smsReciever;


                Sms sms;

                if (nextState == SmsNextStateEnum.SendRequestToSuplier)
                {
                    var suplierList = fridge.Supliers.ToList();
                    var owner = lastSms.FridgeOwner;
                    for (int i = 0; i < suplierList.Count(); i++)
                    {
                        smsType = SmsTypeEnum.TwoWayRequest;
                        smsReciever = SmsRecieverEnum.Suplier;
                        Suplier currentSuplier = suplierList[i];

                        sms = new Sms()
                        {
                            PhoneNumber = currentSuplier.PhoneNumber,
                            Type = smsType.Encode(),
                            Text = string.Format(
                                "Zamówienie dostawy RedBull do Smart Cooler : {0} {1} {2}. Aby potwierdzić przyjęcie zamówienia odpisz: TAK",
                                fridge.FridgeDescription,
                                fridge.Address,
                                owner.PhoneNumber)
                        };

                        string mainMsgId = Send(sms);

                        base.Create(new SmsOutbox()
                        {
                            Fridge = fridge,
                            CreatedDate = DateTime.Now,
                            SmeType = smsType,
                            FridgeOwner = owner,
                            MsgId = mainMsgId,
                            SmsReciever = smsReciever,
                            SmsStatus = SmsStatusEnum.NotRecieved,
                            Suplier = suplierList[i]
                        });
                    }
                }
                if (nextState == SmsNextStateEnum.SendInfoToFridgeOwner)
                {
                    var fridgeOwnerList = fridge.FridgeOwners.ToList();
                    Suplier theFastestSuplier = lastSms.Suplier;
                    for (int i = 0; i < fridgeOwnerList.Count(); i++)
                    {


                        smsType = SmsTypeEnum.OneWayInfo;
                        smsReciever = SmsRecieverEnum.Owner;
                        sms = new Sms()
                        {
                            PhoneNumber = fridgeOwnerList[i].PhoneNumber,
                            Type = smsType.Encode(),
                            Text = "Dostawa RedBull jest już w drodze !"
                        };

                        string mainMsgId = Send(sms);

                        base.Create(new SmsOutbox()
                        {
                            Fridge = fridge,
                            CreatedDate = DateTime.Now,
                            SmeType = smsType,
                            FridgeOwner = fridgeOwnerList[i],
                            MsgId = mainMsgId,
                            SmsReciever = smsReciever,
                            SmsStatus = SmsStatusEnum.NotRecieved,
                            Suplier = theFastestSuplier,
                        });
                    }
                }
                if (nextState == SmsNextStateEnum.SendSuplierToLateInfo)
                {
                    smsType = SmsTypeEnum.OneWayInfo;
                    smsReciever = SmsRecieverEnum.Suplier;
                    sms = new Sms()
                    {
                        PhoneNumber = lastSms.Suplier.PhoneNumber,
                        Type = smsType.Encode(),
                        Text = "Dziękujemy za informację, lecz inny dystrybutor wyruszył z dostawą jako pierwszy."
                    };

                    string mainMsgId = Send(sms);

                    base.Create(new SmsOutbox()
                    {
                        Fridge = fridge,
                        CreatedDate = DateTime.Now,
                        SmeType = smsType,
                        FridgeOwner = null,
                        MsgId = mainMsgId,
                        SmsReciever = smsReciever,
                        SmsStatus = SmsStatusEnum.NotRecieved,
                        Suplier = lastSms.Suplier,
                    });
                }
            }
        }

        private string Send(Sms sms)
        {
            var result = smsFactory
                .ActionSend()
                .SetText(sms.Text)
                .SetTo(sms.PhoneNumber)
                .SetSender(sms.Type)
                .Execute();

            string[] msgIds = new string[result.Count];

            for (int i = 0, l = 0; i < result.List.Count; i++)
            {
                if (!result.List[i].isError())
                {
                    //Nie wystąpił błąd podczas wysyłki (numer|treść|parametry... prawidłowe)
                    if (!result.List[i].isFinal())
                    {
                        //Status nie jest koncowy, może ulec zmianie
                        msgIds[l] = result.List[i].ID;
                        l++;
                    }
                }
            }

            var lastResultStatus = msgIds.Length - 1;
            return msgIds[lastResultStatus];
        }

    }
}