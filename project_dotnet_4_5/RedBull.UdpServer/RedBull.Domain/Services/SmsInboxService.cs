﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedBull.DataBase.Repositories.Generic;
using RedBull.DataBase.Repositories.Interfaces;
using RedBull.DataBase._UnitOfWork;
using RedBull.Domain.Enums;
using RedBull.Domain.Services.Generic;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models;
using RedBull.Entity.Models.Enums;
using SMSApi;
using SMSApi.Api;
using SMSApi.Api.Action;
using SMSApi.Api.Response;

namespace RedBull.Domain.Services
{
    public class SmsInboxService : GenericService<SmsInbox>, ISmsInboxService
    {
        private readonly IGenericRepository<SmsInbox> _smsInboxRepository;
        private readonly IGenericRepository<SmsOutbox> _smsOutboxRepository;
        private readonly IGenericRepository<Fridge> _fridgeRepository;

        public SmsInboxService(IUnitOfWork unitOfWork, IGenericRepository<SmsInbox> _smsInboxRepository,
            IGenericRepository<SmsOutbox> _smsOutboxRepository,IGenericRepository<Fridge> _fridgeRepository)
            : base(unitOfWork, _smsInboxRepository)
        {
            this._smsInboxRepository = _smsInboxRepository;
            this._smsOutboxRepository = _smsOutboxRepository;
            this._fridgeRepository = _fridgeRepository;
        }

        public SmsNextStateEnum? CreateSms(string msgId, string sms_text)
        {
            var convertedText = sms_text.Trim().ToLower();
            var rootSms = _smsOutboxRepository.FindBy(sms => sms.MsgId == msgId).FirstOrDefault();
            if (rootSms == null)
                return null;
            var fridge = _fridgeRepository.GetById(rootSms.FridgeRefId);
            var responseSender = rootSms.SmsReciever;
            if (responseSender==SmsRecieverEnum.Owner)
            {
                var lastActiveOwner = rootSms.FridgeOwner;
                return LogSms(responseSender, fridge, msgId, convertedText, lastActiveOwner, null);
            }

            var lastActiveSuplier = rootSms.Suplier;
            return LogSms(responseSender, fridge, msgId, convertedText, null, lastActiveSuplier);
        }

        private SmsNextStateEnum LogSms(SmsRecieverEnum reciever, Fridge fridge, string msgId, string smsText,
            FridgeOwner fridgeOwner = null, Suplier suplier = null)
        {
            base.Create(new SmsInbox()
            {
                Fridge = fridge,
                MsgId = msgId,
                SmsReciever = reciever,
                SmsText = smsText,
                FridgeOwner = fridgeOwner,
                Suplier = suplier
            });
            
            return GetNextState(reciever,fridge);
        }

        public SmsNextStateEnum GetNextState(SmsRecieverEnum reciever,Fridge fridge)
        {
            var smsFromToday = _smsInboxRepository.FindBy(s => s.Fridge.Id == fridge.Id&&  s.CreatedDate >= DateTime.Today && s.SmsReciever==reciever).Count();
            
            if (reciever == SmsRecieverEnum.Owner)
            {
                
                if (smsFromToday==1)
                {
                    return SmsNextStateEnum.SendRequestToSuplier;
                }
                return SmsNextStateEnum.RequestToSuplierAlreadySended;
            }
            if (reciever == SmsRecieverEnum.Suplier)
            {
                if (smsFromToday==1)
                {
                    return SmsNextStateEnum.SendInfoToFridgeOwner;
                }
                
            }
            return SmsNextStateEnum.SendSuplierToLateInfo;
        }
    }
}
