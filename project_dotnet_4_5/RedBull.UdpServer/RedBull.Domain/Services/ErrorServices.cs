﻿using RedBull.DataBase._UnitOfWork;
using RedBull.DataBase.Repositories.Interfaces;
using RedBull.Domain.Services.Generic;
using RedBull.Domain.Services.Interfaces;
using RedBull.Entity.Models.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedBull.DataBase.Repositories.Generic;

namespace RedBull.Domain.Services
{
    public class ErrorServices : GenericService<ErrorLog>, IErrorServices
    {
        private readonly IGenericRepository<ErrorLog> _errorRepository;

        public ErrorServices(IUnitOfWork unitOfWork, IGenericRepository<ErrorLog> _errorRepository)
            : base(unitOfWork, _errorRepository)
        {
            this._errorRepository = _errorRepository;
        }

        public string GetLastFridgeError(int fridgeId)
        {
            var error = _errorRepository.FindBy(a => a.FridgeRefId==fridgeId).OrderByDescending(a=>a.Id).FirstOrDefault().ErrorText;
            if (error != null)
                return error;
            return "brak błędu dla lodówki o Id" + fridgeId;
        }

        public IEnumerable<ErrorLog> GetFridgeErrorsList(int fridgeId)
        {
            return _errorRepository.FindBy(a => a.Id == fridgeId).ToList();
        }

        public IEnumerable<ErrorLog> GetFridgeErrorsList()
        {
            return _errorRepository.GetAll().ToList();
        }
    }
}