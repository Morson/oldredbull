﻿using RedBull.Entity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Entity.Report
{
    public class GroupReport
    {

        public int AverageNumberOfCans { get; set; }
        
        [Key, Column(Order = 0)]
        public int FridgeId { get; set; }

        [Key, Column(Order = 1)]
        public DateTime ReportDate { get; set; }
        
        [ForeignKey("FridgeId")]
        public virtual Fridge Fridge { get; set; }

    }
}
