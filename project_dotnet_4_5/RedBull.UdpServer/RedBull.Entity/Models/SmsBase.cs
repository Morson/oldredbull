﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedBull.Entity.Models.Enums;

namespace RedBull.Entity.Models
{
    public class SmsBase : Base
    {
        public int FridgeRefId { get; set; }

        [ForeignKey("FridgeRefId")]
        public Fridge Fridge { get; set; }

        public SmsRecieverEnum SmsReciever { get; set; }

        public int? FridgeOwnerRefId { get; set; }

        [ForeignKey("FridgeOwnerRefId")]
        public virtual FridgeOwner FridgeOwner { get; set; }

        public int? SuplierRefId { get; set; }

        [ForeignKey("SuplierRefId")]
        public virtual Suplier Suplier { get; set; }

        public string MsgId { get; set; }
    }
}

