﻿using RedBull.Entity.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedBull.Entity.Models
{
    public class Base : IBase
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }
    }
}
