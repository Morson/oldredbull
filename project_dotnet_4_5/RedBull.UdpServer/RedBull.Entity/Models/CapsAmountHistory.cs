﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Entity.Models
{
    public class CapsAmountHistory:Base
    {
        [Required]
        public int FridgeRefId { get; set; }
        [Required]
        [ForeignKey("FridgeRefId")]
        public Fridge Fridge { get; set; }
        [Required]
        public int ActualAmount { get; set; }

        public decimal ActualWeightOfOneCap { get; set; }
    }
}
