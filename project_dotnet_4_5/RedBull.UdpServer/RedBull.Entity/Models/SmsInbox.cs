﻿using System.ComponentModel.DataAnnotations.Schema;
using RedBull.Entity.Models.Enums;

namespace RedBull.Entity.Models
{
    public class SmsInbox:SmsBase
    {
        public string SmsText { get; set; }
    }
}