﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RedBull.Entity.Models
{
    public class NumberOfCanReport : Base
    {
        [Required]
        [ForeignKey("FridgeRefId")]
        public virtual Fridge Fridge { get; set; }

        [Required]
        public int NumberOfCans { get; set; }

        public int FridgeRefId { get; set; }

        public int TemporaryWeight { get; set; }
    }
}
