﻿namespace RedBull.Entity.Models.Enums
{
    public enum SmsTypeEnum
    {
        TwoWayRequest,
        OneWayInfo,
    }
    public static class EnumExtension

    {
        public static string Encode(this SmsTypeEnum type)
        {
            if (type == SmsTypeEnum.OneWayInfo)
            {
                return "Eco";
            }
            return "2Way";
        }
    }
}