﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RedBull.Entity.Models.Logs;

namespace RedBull.Entity.Models
{
    public class Fridge : Base
    {


        public DateTime LastActivity { get; set; } //DateTime of last fillity report

        [Required]
        public bool FridgeError { get; set; } //If problem with fridge occured

        [Required]
        public bool IsActive { get; set; } //Disabled or enabled

        [Required]
        public int Capacity { get; set; } //Capacity

        public int SallesAvaragePerDay { get; set; }

        public bool IsEmpty { get; set; }

        public string FridgeDescription { get; set; }
        public string ClientName { get; set; }
        public string FridgeManagerName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Address { get; set; }
        public string GoogleLocalization { get; set; }
        public virtual ICollection<FridgeOwner> FridgeOwners { get; set; }
        public virtual ICollection<Suplier> Supliers { get; set; }
        public virtual ICollection<NumberOfCanReport> NumberOfCanReports { get; set; }
        public virtual ICollection<SmsInbox> SmsInboxes { get; set; }
        public virtual ICollection<SmsOutbox> SmsOutboxes { get; set; }
        public virtual ICollection<ErrorLog> ErrorLogs { get; set; }
        public int FridgeWeight { get; set; }
        public decimal TemporaryCanWeight { get; set; }

        public override string ToString()
        {
            return String.Format("Id: {0}, LastActivity: {1}, FridgeError: {2}, IsActive: {3}, Capacity: {4}");
        }
    }
}
