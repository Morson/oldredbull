﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Entity.Models
{
    [Table("log")]
    public class Log
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("pkid")]
        public virtual long Pkid { get; set; }

        [Column("time_stamp")]
        public DateTime TimeStamp { get; set; }

        [MaxLength(255)]
        [Column("logger")]
        public string Logger { get; set; }

        [Column("message")]
        public string Message { get; set; }

        [Column("exception")]
        public string Exception { get; set; }

        [MaxLength(50)]
        [Column("log_level")]
        public string LogLevel { get; set; }

    }
}
