﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Entity.Models
{
    public class Suplier:Base
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public virtual ICollection<Fridge> Fridges { get; set; }

        public virtual ICollection<SmsInbox> SmsInboxes { get; set; }

        public virtual ICollection<SmsOutbox> SmsOutboxes { get; set; }
    }
}
