﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBull.Entity.Models
{
    public class FridgeOwner:Base
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Surname { get; set; }

        public virtual ICollection<SmsInbox> SmsInboxes { get; set; }

        public virtual ICollection<SmsOutbox> SmsOutboxes { get; set; }

        public virtual ICollection<Fridge> Fridges { get; set; } 
    }
}
