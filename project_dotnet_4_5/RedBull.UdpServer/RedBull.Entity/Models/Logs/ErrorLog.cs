﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RedBull.Entity.Models.Logs
{
    public class ErrorLog : Base
    {
        public string ErrorText { get; set; }

        public int? FridgeRefId { get; set; }

        [ForeignKey("FridgeRefId")]
        public virtual Fridge Fridge { get; set; }
    }
}
