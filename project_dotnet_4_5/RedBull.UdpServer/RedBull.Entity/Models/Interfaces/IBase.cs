﻿using System;

namespace RedBull.Entity.Models.Interfaces
{
    public interface IBase
    {
        DateTime CreatedDate { get; set; }
        int Id { get; set; }
    }
}
