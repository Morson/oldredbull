﻿using RedBull.Entity.Models.Enums;

namespace RedBull.Entity.Models
{
    public class SmsOutbox:SmsBase
    {
        public SmsTypeEnum SmeType { get; set; }

        public SmsStatusEnum SmsStatus { get; set; }
    }
}