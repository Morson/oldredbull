namespace Redbull.DAL.MySql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Logger : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "log",
                c => new
                    {
                        pkid = c.Long(nullable: false, identity: true),
                        time_stamp = c.DateTime(nullable: false, precision: 0),
                        logger = c.String(maxLength: 255, storeType: "nvarchar"),
                        message = c.String(unicode: false),
                        exception = c.String(unicode: false),
                        log_level = c.String(maxLength: 50, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.pkid);
            
        }
        
        public override void Down()
        {
            DropTable("log");
        }
    }
}
