namespace Redbull.DAL.MySql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DiffMinutes : DbMigration
    {
        public override void Up()
        {
            Sql("CREATE FUNCTION `DiffMinutes`(actualDateTime datetime, nextDateTime datetime) " +
                "RETURNS datetime " +
                "BEGIN " +
                "RETURN TIMESTAMPDIFF(MINUTE, actualDateTime, nextDateTime); " +
                "END");
        }
        
        public override void Down()
        {
            Sql("DROP FUNCTION `DiffMinutes`");
        }
    }
}
