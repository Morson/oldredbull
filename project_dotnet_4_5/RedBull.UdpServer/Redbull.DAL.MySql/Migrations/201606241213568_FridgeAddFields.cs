namespace Redbull.DAL.MySql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FridgeAddFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("Fridge", "SallesAvaragePerDay", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Fridge", "SallesAvaragePerDay");
        }
    }
}
