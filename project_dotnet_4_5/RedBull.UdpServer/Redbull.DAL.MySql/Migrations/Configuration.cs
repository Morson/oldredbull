using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using MySql.Data.Entity;

namespace Redbull.DAL.MySql.Migrations
{


    internal sealed class Configuration : DbMigrationsConfiguration<Redbull.DAL.MySql.Context.MySqlRedbullContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            SetSqlGenerator("MySql.Data.MySqlClient", new MySqlMigrationSqlGenerator());
        }

        protected override void Seed(Redbull.DAL.MySql.Context.MySqlRedbullContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
