namespace Redbull.DAL.MySql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FridgeChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("Fridge", "IsEmpty", c => c.Boolean(nullable: false));
            AddColumn("Fridge", "FridgeDescription", c => c.String(unicode: false));
            AddColumn("Fridge", "ClientName", c => c.String(unicode: false));
            AddColumn("Fridge", "FridgeManagerName", c => c.String(unicode: false));
            AddColumn("Fridge", "PhoneNumber", c => c.String(unicode: false));
            AddColumn("Fridge", "EmailAddress", c => c.String(unicode: false));
            AddColumn("Fridge", "Address", c => c.String(unicode: false));
            AddColumn("Fridge", "GoogleLocalization", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("Fridge", "GoogleLocalization");
            DropColumn("Fridge", "Address");
            DropColumn("Fridge", "EmailAddress");
            DropColumn("Fridge", "PhoneNumber");
            DropColumn("Fridge", "FridgeManagerName");
            DropColumn("Fridge", "ClientName");
            DropColumn("Fridge", "FridgeDescription");
            DropColumn("Fridge", "IsEmpty");
        }
    }
}
