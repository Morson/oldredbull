namespace Redbull.DAL.MySql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupReportViews : DbMigration
    {
        public override void Up()
        {
            Sql("CREATE FUNCTION `RoundDatetime`(`p_datetime` datetime, `p_type` INT) " +
                "RETURNS datetime " +
                "LANGUAGE SQL " +
                "DETERMINISTIC " +
                "CONTAINS SQL " +
                "BEGIN " +
                " DECLARE ratio, unit, counter, current_unit int; " +
                " DECLARE minutes int; " +
                " DECLARE p_date date; " +
                " DECLARE hour_part int; " +
                " SET hour_part = DATE_FORMAT(p_datetime, '%k'); " +
                " SET minutes = DATE_FORMAT(p_datetime, '%i'); " +
                " SET p_date = DATE(p_datetime); " +
                " SET ratio = 60 / p_type; " +
                " SET unit = 60 / ratio; " +
                " SET counter = 0; " +
                " WHILE counter < ratio DO " +
                " SET current_unit = counter * unit; " +
                " IF minutes <= current_unit THEN " +
                " RETURN (CONCAT(p_date, ' ', hour_part, ':', current_unit, ':00')); " +
                " END IF; " +
                " SET counter = counter + 1; " +
                " END WHILE; " +
                " SET hour_part = hour_part + 1; " +
                " IF hour_part > 23 THEN " +
                " SET hour_part = 0; " +
                " SET p_date = DATE_ADD(p_date, INTERVAL 1 DAY); " +
                " END IF;    " +
                " RETURN(CONCAT(p_date, ' ', hour_part, ':00:00')); " +
                " END ");


            Sql("create view `GroupReport15View` AS select max(`report`.`NumberOfCans`) AS `MaxNumberOfCans`,`RoundDatetime`(`report`.`CreatedDate`,15) AS `ReportDate`,`report`.`Fridge_Id` AS `FridgeId` from `NumberOfCanReport` `report` group by `report`.`Fridge_Id`,`RoundDatetime`(`report`.`CreatedDate`,15)");
            Sql("create view `GroupReport30View` AS select max(`report`.`NumberOfCans`) AS `MaxNumberOfCans`,`RoundDatetime`(`report`.`CreatedDate`,30) AS `ReportDate`,`report`.`Fridge_Id` AS `FridgeId` from `NumberOfCanReport` `report` group by `report`.`Fridge_Id`,`RoundDatetime`(`report`.`CreatedDate`,30)");
            Sql("create view `GroupReport60View` AS select max(`report`.`NumberOfCans`) AS `MaxNumberOfCans`,`RoundDatetime`(`report`.`CreatedDate`,60) AS `ReportDate`,`report`.`Fridge_Id` AS `FridgeId` from `NumberOfCanReport` `report` group by `report`.`Fridge_Id`,`RoundDatetime`(`report`.`CreatedDate`,60)");
        }
        
        public override void Down()
        {

        }
    }
}
