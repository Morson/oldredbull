namespace Redbull.DAL.MySql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Fridge",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FridgeIdentity = c.Int(nullable: false),
                        LastActivity = c.DateTime(nullable: false, precision: 0),
                        FridgeError = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Capacity = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.FridgeIdentity, unique: true);
            
            CreateTable(
                "NumberOfCanReport",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NumberOfCans = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false, precision: 0),
                        Fridge_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Fridge", t => t.Fridge_Id, cascadeDelete: true)
                .Index(t => t.Fridge_Id);
            
            CreateTable(
                "ErrorLog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ErrorText = c.String(unicode: false),
                        FridgeName = c.String(unicode: false),
                        CreatedDate = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("NumberOfCanReport", "Fridge_Id", "Fridge");
            DropIndex("NumberOfCanReport", new[] { "Fridge_Id" });
            DropIndex("Fridge", new[] { "FridgeIdentity" });
            DropTable("ErrorLog");
            DropTable("NumberOfCanReport");
            DropTable("Fridge");
        }
    }
}
