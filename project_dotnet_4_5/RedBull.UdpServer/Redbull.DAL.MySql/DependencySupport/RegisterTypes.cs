﻿using Microsoft.Practices.Unity;
using Redbull.DAL.MySql.Context;
using RedBull.DataBase._UnitOfWork;
using RedBull.DataBase.Context;
using RedBull.DataBase.Repositories;
using RedBull.DataBase.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redbull.DAL.MySql.DependencySupport
{
    public class RegisterTypes
    {

        private readonly IUnityContainer unityContainer;

        public RegisterTypes(IUnityContainer unityContainer)
        {
            this.unityContainer = unityContainer;
        }

        public IUnityContainer BuildUnityContainer()
        {
            //Repository
            //unityContainer.RegisterType<IErrorRepository, ErrorRepository>();
            //unityContainer.RegisterType<IFridgeRpository, FridgeRpository>();
            unityContainer.RegisterType<IReportRepository, ReportRepository>();

            //context
            unityContainer.RegisterType<IRedBullContext, MySqlRedbullContext>(new PerResolveLifetimeManager());

            //UnitOfWork
            unityContainer.RegisterType<IUnitOfWork, UnitOfWork>();

            return unityContainer;
        }

    }
}
