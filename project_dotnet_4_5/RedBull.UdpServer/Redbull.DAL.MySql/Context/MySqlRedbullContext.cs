﻿using Redbull.DAL.MySql.Migrations;
using RedBull.DataBase.Context;
using RedBull.Entity.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data.Entity;
using RedBull.Entity.Models;
using RedBull.Entity.Report;

namespace Redbull.DAL.MySql.Context
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class MySqlRedbullContext : DbContext, IRedBullContext
    {

        public MySqlRedbullContext()
            : base("name=RedBull_DB_ConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MySqlRedbullContext, Configuration>("RedBull_DB_ConnectionString"));
        }

        public DbSet<RedBull.Entity.Models.Fridge> Fridges
        {
            get;
            set;
        }

        public DbSet<RedBull.Entity.Models.NumberOfCanReport> NumberOfCanReports
        {
            get;
            set;
        }

        public DbSet<FridgeOwner> FridgeOwners { get; set; }
        public DbSet<Suplier> Supliers { get; set; }
        public DbSet<SmsInbox> SmsInboxes { get; set; }
        public DbSet<SmsOutbox> SmsOutboxes { get; set; }
        public DbSet<CapsAmountHistory> CapsAmountHistories { get; set; }
        public DbSet<RedBull.Entity.Models.Logs.ErrorLog> ErrorLogs
        {
            get;
            set;
        }

        public DbSet<Log> Logs
        {
            get;
            set;
        }

        public DbSet<GroupReport15View> GroupReport15
        {
            get; set;
        }

        public DbSet<GroupReport30View> GroupReport30
        {
            get; set;
        }

        public DbSet<GroupReport60View> GroupReport60
        {
            get; set;
        }

        public DbSet<GroupDayReportView> GroupDayReport
        {
            get; set;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
         
        }

        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IBase
                    && (x.State == System.Data.Entity.EntityState.Added
                        || x.State == System.Data.Entity.EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                IBase entity = entry.Entity as IBase;
                if (entity != null)
                {
                    string identityName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime now = DateTime.Now;

                    if (entry.State == System.Data.Entity.EntityState.Added)
                    {
                        entity.CreatedDate = now;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }
                }
            }

            return base.SaveChanges();
        }


        
    }
}
